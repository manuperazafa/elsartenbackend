<?php
App::uses('AppModel', 'Model');
/**
 * Cliente Model
 *
 * @property Vendedore $Vendedore
 * @property Zona $Zona
 */
class Cliente extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Vendedore' => array(
			'className' => 'Vendedore',
			'foreignKey' => 'co_ven',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Zona' => array(
			'className' => 'Zona',
			'foreignKey' => 'co_zon',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	public $hasMany = array(		
		'Cita' => array(
			'className' => 'Cita',
			'foreignKey' => 'cliente_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Cotizacione' => array(
			'className' => 'Cotizacione',
			'foreignKey' => 'co_cli',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);


}
