<?php
App::uses('AppModel', 'Model');
/**
 * Factura Model
 *
 * @property categoria $Factura
 * @property Factura $Factura
 */
class Factura extends AppModel {


	public $hasOne = array (
		'Cliente' => array(
			'className' => 'Cliente',
			'foreignKey' => false,
             'conditions' => array(
                 'RTRIM(Factura.co_cli) = RTRIM(Cliente.co_cli)'
             )
		)
	);
}
