<div class="articulos index">
	<h2><?php echo __('Articulos'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('co_art'); ?></th>
			<th><?php echo $this->Paginator->sort('art_des'); ?></th>
			<th><?php echo $this->Paginator->sort('fecha_reg'); ?></th>
			<th><?php echo $this->Paginator->sort('manj_ser'); ?></th>
			<th><?php echo $this->Paginator->sort('co_lin'); ?></th>
			<th><?php echo $this->Paginator->sort('co_cat'); ?></th>
			<th><?php echo $this->Paginator->sort('co_subl'); ?></th>
			<th><?php echo $this->Paginator->sort('co_color'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($articulos as $articulo): ?>
	<tr>
		<td><?php echo h($articulo['Articulo']['id']); ?>&nbsp;</td>
		<td><?php echo h($articulo['Articulo']['co_art']); ?>&nbsp;</td>
		<td><?php echo h($articulo['Articulo']['art_des']); ?>&nbsp;</td>
		<td><?php echo h($articulo['Articulo']['fecha_reg']); ?>&nbsp;</td>
		<td><?php echo h($articulo['Articulo']['manj_ser']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($articulo['lin']['id'], array('controller' => 'lins', 'action' => 'view', $articulo['lin']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($articulo['categoria']['id'], array('controller' => 'categorias', 'action' => 'view', $articulo['categoria']['id'])); ?>
		</td>
		<td><?php echo h($articulo['Articulo']['co_subl']); ?>&nbsp;</td>
		<td><?php echo h($articulo['Articulo']['co_color']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $articulo['Articulo']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $articulo['Articulo']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $articulo['Articulo']['id']), array(), __('Are you sure you want to delete # %s?', $articulo['Articulo']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Articulo'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Categorias'), array('controller' => 'categorias', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Categoria'), array('controller' => 'categorias', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Lins'), array('controller' => 'lins', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Lin'), array('controller' => 'lins', 'action' => 'add')); ?> </li>
	</ul>
</div>
