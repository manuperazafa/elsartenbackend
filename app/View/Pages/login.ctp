<div class="columns-container">
   <div id="columns" class="container">
      <div class="breadcrumb clearfix">
         <a class="home" href="http://livedemo00.template-help.com/prestashop_53429/" title="Volver a Inicio">
         <i class="fa fa-home"></i>
         </a>
         <span class="navigation-pipe">&gt;</span>
         Autenticación
      </div>
      <div class="row">
         <div id="left_column" class="column col-xs-12 col-sm-3">
            <?php echo $this->element('bestSeller');?>
            <section id="wishlist_block" class="block account">
               <h4 class="title_block">
                  <a href="http://livedemo00.template-help.com/prestashop_53429/index.php?fc=module&amp;module=blockwishlist&amp;controller=mywishlist&amp;id_lang=4" title="Mi lista de regalos" rel="nofollow">
                  Lista de deseos
                  </a>
               </h4>
               <div class="block_content" style="">
                  <div id="wishlist_block_list" class="expanded">
                     <dl class="products no-products">
                        <dt>Ningún producto</dt>
                        <dd></dd>
                     </dl>
                  </div>
                  <div class="lnk">
                     <a class="btn btn-default btn-sm icon-right" href="http://livedemo00.template-help.com/prestashop_53429/index.php?fc=module&amp;module=blockwishlist&amp;controller=mywishlist&amp;id_lang=4" title="Mi lista de regalos">
                     <span>
                     Mi lista de regalos
                     </span>
                     </a>
                  </div>
               </div>
            </section>
            <section id="special_block_right" class="block">
               <h4 class="title_block">
                  <a href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=prices-drop" title="Promociones especiales">
                  Promociones especiales
                  </a>
               </h4>
               <div class="block_content products-block" style="">
                  <ul>
                     <li class="clearfix">
                        <a class="products-block-image" href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=1&amp;controller=product&amp;id_lang=4">
                        <img class="replace-2x img-responsive" src="http://livedemo00.template-help.com/prestashop_53429/img/p/2/4/1/241-tm_small_default.jpg" alt="Nesting-Mixing-Bowl-Set" title="Nesting-Mixing-Bowl-Set">
                        </a>
                        <div class="product-content">
                           <h5>
                              <a class="product-name" href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=1&amp;controller=product&amp;id_lang=4" title="Nesting-Mixing-Bowl-Set">
                              Nesting-Mixing-Bowl-Set
                              </a>
                           </h5>
                           <p class="product-description">
                              Kitchen Supplies store was founded by...
                           </p>
                           <div class="price-box">
                              <span class="price special-price">
                              $24.00
                              </span>
                              <span class="price-percent-reduction">-20%</span>
                              <span class="old-price">
                              $30.00
                              </span>
                           </div>
                        </div>
                     </li>
                  </ul>
                  <div>
                     <a class="btn btn-default btn-sm icon-right" href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=prices-drop" title="Todas los promociones especiales">
                     <span>
                     Todas los promociones especiales
                     </span>
                     </a>
                  </div>
               </div>
            </section>
            <section id="suppliers_block_left" class="block blocksupplier">
               <h4 class="title_block">
                  <a href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=supplier" title="Proveedores">
                  Proveedores
                  </a>
               </h4>
               <div class="block_content list-block" style="">
                  <ul>
                     <li class="first_item">
                        <a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=3&amp;controller=supplier&amp;id_lang=4" title="Más sobre Consectetur adipiscing elit">
                        Consectetur adipiscing elit
                        </a>
                     </li>
                     <li class="item">
                        <a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=1&amp;controller=supplier&amp;id_lang=4" title="Más sobre Curabitur ac ipsum pellentesque">
                        Curabitur ac ipsum pellentesque
                        </a>
                     </li>
                     <li class="item">
                        <a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=6&amp;controller=supplier&amp;id_lang=4" title="Más sobre Interdum et malesuada">
                        Interdum et malesuada
                        </a>
                     </li>
                     <li class="item">
                        <a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=2&amp;controller=supplier&amp;id_lang=4" title="Más sobre Lorem ipsum dolor sit amet">
                        Lorem ipsum dolor sit amet
                        </a>
                     </li>
                     <li class="item">
                        <a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=7&amp;controller=supplier&amp;id_lang=4" title="Más sobre Morbi in volutpat eros">
                        Morbi in volutpat eros
                        </a>
                     </li>
                  </ul>
                  <form action="/prestashop_53429/index.php" method="get">
                     <div class="form-group selector1">
                        <div class="selector" style="width: 157px;">
                           <span style="width: 145px; -webkit-user-select: none;">Todos los proveedores</span>
                           <select class="form-control" name="supplier_list">
                              <option value="0">Todos los proveedores</option>
                              <option value="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=3&amp;controller=supplier&amp;id_lang=4">Consectetur adipiscing elit</option>
                              <option value="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=1&amp;controller=supplier&amp;id_lang=4">Curabitur ac ipsum pellentesque</option>
                              <option value="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=6&amp;controller=supplier&amp;id_lang=4">Interdum et malesuada</option>
                              <option value="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=2&amp;controller=supplier&amp;id_lang=4">Lorem ipsum dolor sit amet</option>
                              <option value="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=7&amp;controller=supplier&amp;id_lang=4">Morbi in volutpat eros</option>
                              <option value="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=5&amp;controller=supplier&amp;id_lang=4">Nullam aliquam odio tellus</option>
                              <option value="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=8&amp;controller=supplier&amp;id_lang=4">Sed imperdiet porttitor nulla</option>
                              <option value="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=4&amp;controller=supplier&amp;id_lang=4">Ut feugiat lobortis</option>
                           </select>
                        </div>
                     </div>
                  </form>
               </div>
            </section>
            <section id="informations_block_left_1" class="block informations_block_left">
               <h4 class="title_block">
                  <a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_cms_category=1&amp;controller=cms&amp;id_lang=4" title="Information">
                  Information </a>
               </h4>
               <div class="block_content list-block" style="">
                  <ul>
                     <li>
                        <a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_cms=1&amp;controller=cms&amp;id_lang=4" title="Delivery">
                        Delivery
                        </a>
                     </li>
                     <li>
                        <a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_cms=2&amp;controller=cms&amp;id_lang=4" title="Legal Notice">
                        Legal Notice
                        </a>
                     </li>
                     <li>
                        <a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_cms=3&amp;controller=cms&amp;id_lang=4" title="Terms and conditions of use">
                        Terms and conditions of use
                        </a>
                     </li>
                     <li>
                        <a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_cms=4&amp;controller=cms&amp;id_lang=4" title="About us">
                        About us
                        </a>
                     </li>
                     <li>
                        <a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_cms=5&amp;controller=cms&amp;id_lang=4" title="Secure payment">
                        Secure payment
                        </a>
                     </li>
                     <li>
                        <a href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=stores" title="Nuestras tiendas">
                        Nuestras tiendas
                        </a>
                     </li>
                  </ul>
               </div>
            </section>
         </div>
         <div id="center_column" class="center_column col-xs-12 col-sm-9">
            <h1 class="page-heading">Autenticación</h1>
            <div class="row">
               <div class="col-xs-12 col-sm-6">
                  <form action="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=authentication" method="post" id="create-account_form" class="box">
                     <h3 class="page-subheading">Crear una cuenta</h3>
                     <div class="form_content clearfix">
                        <p>Escriba su correo electrónico para crear su cuenta</p>
                        <div class="alert alert-danger" id="create_account_error" style="display:none"></div>
                        <div class="form-group">
                           <label for="email_create">Correo electrónico</label>
                           <input type="text" class="is_required validate account_input form-control" data-validate="isEmail" id="email_create" name="email_create" value="">
                        </div>
                        <div class="submit">
                           <input type="hidden" class="hidden" name="back" value="my-account"> <button class="btn btn-default btn-md" type="submit" id="SubmitCreate" name="SubmitCreate">
                           <span>
                           <i class="fa fa-user left"></i>
                           Crear una cuenta
                           </span>
                           </button>
                           <input type="hidden" class="hidden" name="SubmitCreate" value="Crear una cuenta">
                        </div>
                     </div>
                  </form>
               </div>
               <div class="col-xs-12 col-sm-6">
                  <form action="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=authentication" method="post" id="login_form" class="box">
                     <h3 class="page-subheading">¿Ya está registrado?</h3>
                     <div class="form_content clearfix">
                        <div class="form-group">
                           <label for="email">Correo electrónico</label>
                           <input class="is_required validate account_input form-control" data-validate="isEmail" type="text" id="email" name="email" value="">
                        </div>
                        <div class="form-group">
                           <label for="passwd">Contraseña</label>
                           <span>
                           <input class="is_required validate account_input form-control" type="password" data-validate="isPasswd" id="passwd" name="passwd" value="">
                           </span>
                        </div>
                        <p class="lost_password form-group">
                           <a href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=password" title="Recuperar la contraseña" rel="nofollow">¿Olvidó su contraseña?</a>
                        </p>
                        <p class="submit">
                           <input type="hidden" class="hidden" name="back" value="my-account">
                           <button type="submit" id="SubmitLogin" name="SubmitLogin" class="btn btn-default btn-md">
                           <span>
                           <i class="fa fa-lock left"></i>
                           Iniciar sesión
                           </span>
                           </button>
                        </p>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
