<div id="top_column">
         <?php echo $this->element('home-slider');?>
         <?php echo $this->element('ofertas');?>
</div>
<div id="page">
  
   <div class="columns-container">
      <div id="columns" class="container">
         <div class="row">
            <div id="center_column" class="center_column col-xs-12 col-sm-12">
               <ul id="home-page-tabs" class="nav nav-tabs clearfix">
                  <li class="homefeatured active"><a data-toggle="tab" href="#homefeatured" class="homefeatured">Populares</a></li>
                  <li class="blocknewproducts"><a data-toggle="tab" href="#blocknewproducts" class="blocknewproducts">Nuevos</a></li>
                  <li class="blockbestsellers"><a data-toggle="tab" href="#blockbestsellers" class="blockbestsellers">Los más vendidos</a></li>
               </ul>
               <div class="tab-content">
                  <div class="bx-wrapper" style="max-width: 1690px; margin: 0px auto;">
                     <div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: 425px;">
                        <ul id="homepage-carousel" class="homepage-carousel product_list grid" style="width: 2215%; position: relative; left: -722px;" >
                           <li class="ajax_block_product col-xs-12 col-sm-4 col-md-3 first-in-line last-line first-item-of-mobile-line bx-clone" style="float: left; list-style: none; position: relative; margin-right: 30px; opacity: 1; transform: translate3d(0px, 0px, 0px); width: 150.5px;">
                              <div class="product-container" itemscope="" itemtype="http://schema.org/Product">
                                 <div class="left-block">
                                    <div class="product-image-container">
                                       <a class="product_img_link" href="index.php?id_product=17&amp;controller=product&amp;id_lang=4" title="Maple-Cutting-Board" itemprop="url">
                                       <img class="replace-2x img-responsive" src="img/334-tm_home_default.jpg" alt="Maple-Cutting-Board" title="Maple-Cutting-Board" itemprop="image">
                                       </a>
                                       <ul class="gallery-thumb-list">
                                          <li id="thumb-17-334" class="gallery-image-thumb active">
                                             <a href="index.php?id_product=17&amp;controller=product&amp;id_lang=4" title="Maple-Cutting-Board" data-href="img/334-tm_home_default.jpg">
                                             <img class="img-responsive" id="thumb-334" src="img/334-tm_cart_default.jpg" alt="Maple-Cutting-Board" title="Maple-Cutting-Board" itemprop="image">
                                             </a>
                                          </li>
                                          <li id="thumb-17-335" class="gallery-image-thumb">
                                             <a href="index.php?id_product=17&amp;controller=product&amp;id_lang=4" title="Maple-Cutting-Board" data-href="img/335-tm_home_default.jpg">
                                             <img class="img-responsive" id="thumb-335" src="img/335-tm_cart_default.jpg" alt="Maple-Cutting-Board" title="Maple-Cutting-Board" itemprop="image">
                                             </a>
                                          </li>
                                          <li id="thumb-17-336" class="gallery-image-thumb">
                                             <a href="index.php?id_product=17&amp;controller=product&amp;id_lang=4" title="Maple-Cutting-Board" data-href="img/336-tm_home_default.jpg">
                                             <img class="img-responsive" id="thumb-336" src="img/336-tm_cart_default.jpg" alt="Maple-Cutting-Board" title="Maple-Cutting-Board" itemprop="image">
                                             </a>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                                 <div class="right-block">
                                    <h5 itemprop="name">
                                       <a class="product-name" href="index.php?id_product=17&amp;controller=product&amp;id_lang=4" title="Maple-Cutting-Board" itemprop="url">
                                       <span class="list-name">Maple-Cutting-Board</span>
                                       <span class="grid-name">Maple-Cutting-Board</span>
                                       </a>
                                    </h5>
                                    <p class="product-desc" itemprop="description">
                                       <span class="list-desc">Kitchen Supplies store was founded by several enthusiasts in 2002. Those were the times when people still preferred to buy products at brick-and-mortar stores instead of buying online.</span>
                                       <span class="grid-desc">Kitchen Supplies store was founded by...</span>
                                    </p>
                                    <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="content_price">
                                       <span itemprop="price" class="price product-price">
                                       $70.00 </span>
                                       <meta itemprop="priceCurrency" content="USD">
                                    </div>
                                    <div class="bottom_product_box">
                                       <div class="button-container">
                                          <a class="ajax_add_to_cart_button btn btn-default" href="index.php?controller=cart&amp;add=1&amp;id_product=17&amp;token=01ade8a68c5202cd5134eb506a06dd04" rel="nofollow" title="Añadir al carrito" data-id-product="17" data-minimal_quantity="1">
                                          <span>Añadir al carrito</span>
                                          </a>
                                          <a itemprop="url" class="lnk_view btn btn-default" href="index.php?id_product=17&amp;controller=product&amp;id_lang=4" title="Ver">
                                          <span>Más</span>
                                          </a>
                                       </div>
                                       <div class="comments_note" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                                          <div class="star_content clearfix">
                                             <div class="star star_on"></div>
                                             <div class="star star_on"></div>
                                             <div class="star star_on"></div>
                                             <div class="star star_on"></div>
                                             <div class="star star_on"></div>
                                             <meta itemprop="worstRating" content="0">
                                             <meta itemprop="ratingValue" content="5">
                                             <meta itemprop="bestRating" content="5">
                                          </div>
                                          <span class="nb-comments"><span itemprop="reviewCount">1</span> Comentario(s)</span>
                                       </div>
                                       <div class="product-flags"></div>
                                       <span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="availability">
                                          <span class="available-now">
                                             <link itemprop="availability" href="http://schema.org/InStock">
                                             En stock 
                                          </span>
                                       </span>
                                       <div class="more_qv">
                                          <div class="more_qv_buttons quick-view-box">
                                             <a class="quick-view" href="index.php?id_product=17&amp;controller=product&amp;id_lang=4" rel="index.php?id_product=17&amp;controller=product&amp;id_lang=4">
                                             <i class="fa fa-search"></i><span>Vista rápida</span>
                                             </a>
                                          </div>
                                          <div class="more_qv_buttons button_Box">
                                             <a itemprop="url" class="moreBox " href="index.php?id_product=17&amp;controller=product&amp;id_lang=4" title="Ver">
                                             <i class="fa fa-info"></i><span>Más</span> </a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li class="ajax_block_product col-xs-12 col-sm-4 col-md-3 first-in-line last-item-of-tablet-line first-item-of-mobile-line" style="float: left; list-style: none; position: relative; margin-right: 30px; opacity: 1; transform: translate3d(0px, 0px, 0px); width: 150.5px;">
                              <div class="product-container" itemscope="" itemtype="http://schema.org/Product">
                                 <div class="left-block">
                                    <div class="product-image-container">
                                       <a class="product_img_link" href="index.php?id_product=9&amp;controller=product&amp;id_lang=4" title="Butter-Maker" itemprop="url">
                                       <img class="replace-2x img-responsive" src="img/286-tm_home_default.jpg" alt="Butter-Maker" title="Butter-Maker" itemprop="image">
                                       </a>
                                       <ul class="gallery-thumb-list">
                                          <li id="thumb-9-286" class="gallery-image-thumb active">
                                             <a href="index.php?id_product=9&amp;controller=product&amp;id_lang=4" title="Butter-Maker" data-href="img/286-tm_home_default.jpg">
                                             <img class="img-responsive" id="thumb-286" src="img/286-tm_cart_default.jpg" alt="Butter-Maker" title="Butter-Maker" itemprop="image">
                                             </a>
                                          </li>
                                          <li id="thumb-9-287" class="gallery-image-thumb">
                                             <a href="index.php?id_product=9&amp;controller=product&amp;id_lang=4" title="Butter-Maker" data-href="img/287-tm_home_default.jpg">
                                             <img class="img-responsive" id="thumb-287" src="img/287-tm_cart_default.jpg" alt="Butter-Maker" title="Butter-Maker" itemprop="image">
                                             </a>
                                          </li>
                                          <li id="thumb-9-288" class="gallery-image-thumb">
                                             <a href="index.php?id_product=9&amp;controller=product&amp;id_lang=4" title="Butter-Maker" data-href="img/288-tm_home_default.jpg">
                                             <img class="img-responsive" id="thumb-288" src="img/288-tm_cart_default.jpg" alt="Butter-Maker" title="Butter-Maker" itemprop="image">
                                             </a>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                                 <div class="right-block">
                                    <h5 itemprop="name">
                                       <a class="product-name" href="index.php?id_product=9&amp;controller=product&amp;id_lang=4" title="Butter-Maker" itemprop="url">
                                       <span class="list-name">Butter-Maker</span>
                                       <span class="grid-name">Butter-Maker</span>
                                       </a>
                                    </h5>
                                    <p class="product-desc" itemprop="description">
                                       <span class="list-desc">Kitchen Supplies store was founded by several enthusiasts in 2002. Those were the times when people still preferred to buy products at brick-and-mortar stores instead of buying online.&nbsp;</span>
                                       <span class="grid-desc">Kitchen Supplies store was founded by...</span>
                                    </p>
                                    <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="content_price">
                                       <span itemprop="price" class="price product-price">
                                       $15.00 </span>
                                       <meta itemprop="priceCurrency" content="USD">
                                    </div>
                                    <div class="bottom_product_box">
                                       <div class="button-container">
                                          <a class="ajax_add_to_cart_button btn btn-default" href="index.php?controller=cart&amp;add=1&amp;id_product=9&amp;token=01ade8a68c5202cd5134eb506a06dd04" rel="nofollow" title="Añadir al carrito" data-id-product="9" data-minimal_quantity="1">
                                          <span>Añadir al carrito</span>
                                          </a>
                                          <a itemprop="url" class="lnk_view btn btn-default" href="index.php?id_product=9&amp;controller=product&amp;id_lang=4" title="Ver">
                                          <span>Más</span>
                                          </a>
                                       </div>
                                       <div class="comments_note" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                                          <div class="star_content clearfix">
                                             <div class="star star_on"></div>
                                             <div class="star star_on"></div>
                                             <div class="star star_on"></div>
                                             <div class="star star_on"></div>
                                             <div class="star star_on"></div>
                                             <meta itemprop="worstRating" content="0">
                                             <meta itemprop="ratingValue" content="5">
                                             <meta itemprop="bestRating" content="5">
                                          </div>
                                          <span class="nb-comments"><span itemprop="reviewCount">1</span> Comentario(s)</span>
                                       </div>
                                       <div class="product-flags"></div>
                                       <span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="availability">
                                          <span class="available-now">
                                             <link itemprop="availability" href="http://schema.org/InStock">
                                             En stock 
                                          </span>
                                       </span>
                                       <div class="more_qv">
                                          <div class="more_qv_buttons quick-view-box">
                                             <a class="quick-view" href="index.php?id_product=9&amp;controller=product&amp;id_lang=4" rel="index.php?id_product=9&amp;controller=product&amp;id_lang=4">
                                             <i class="fa fa-search"></i><span>Vista rápida</span>
                                             </a>
                                          </div>
                                          <div class="more_qv_buttons button_Box">
                                             <a itemprop="url" class="moreBox " href="index.php?id_product=9&amp;controller=product&amp;id_lang=4" title="Ver">
                                             <i class="fa fa-info"></i><span>Más</span> </a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </li>
                         
                           <li class="ajax_block_product col-xs-12 col-sm-4 col-md-3 last-item-of-mobile-line bx-clone" style="float: left; list-style: none; position: relative; margin-right: 30px; opacity: 1; transform: translate3d(0px, 0px, 0px); width: 150.5px;">
                              <div class="product-container" itemscope="" itemtype="http://schema.org/Product">
                                 <div class="left-block">
                                    <div class="product-image-container">
                                       <a class="product_img_link" href="index.php?id_product=2&amp;controller=product&amp;id_lang=4" title="Glass-Nesting-Bowl-Set" itemprop="url">
                                       <img class="replace-2x img-responsive" src="img/244-tm_home_default.jpg" alt="Glass-Nesting-Bowl-Set" title="Glass-Nesting-Bowl-Set" itemprop="image">
                                       </a>
                                       <ul class="gallery-thumb-list">
                                          <li id="thumb-2-244" class="gallery-image-thumb active">
                                             <a href="index.php?id_product=2&amp;controller=product&amp;id_lang=4" title="Glass-Nesting-Bowl-Set" data-href="img/244-tm_home_default.jpg">
                                             <img class="img-responsive" id="thumb-244" src="img/244-tm_cart_default.jpg" alt="Glass-Nesting-Bowl-Set" title="Glass-Nesting-Bowl-Set" itemprop="image">
                                             </a>
                                          </li>
                                          <li id="thumb-2-245" class="gallery-image-thumb">
                                             <a href="index.php?id_product=2&amp;controller=product&amp;id_lang=4" title="Glass-Nesting-Bowl-Set" data-href="img/245-tm_home_default.jpg">
                                             <img class="img-responsive" id="thumb-245" src="img/245-tm_cart_default.jpg" alt="Glass-Nesting-Bowl-Set" title="Glass-Nesting-Bowl-Set" itemprop="image">
                                             </a>
                                          </li>
                                          <li id="thumb-2-246" class="gallery-image-thumb">
                                             <a href="index.php?id_product=2&amp;controller=product&amp;id_lang=4" title="Glass-Nesting-Bowl-Set" data-href="img/246-tm_home_default.jpg">
                                             <img class="img-responsive" id="thumb-246" src="img/246-tm_cart_default.jpg" alt="Glass-Nesting-Bowl-Set" title="Glass-Nesting-Bowl-Set" itemprop="image">
                                             </a>
                                          </li>
                                       </ul>
                                       <a class="sale-box" href="index.php?id_product=2&amp;controller=product&amp;id_lang=4">
                                       <span class="sale-label">¡Oferta!</span>
                                       </a>
                                    </div>
                                 </div>
                                 <div class="right-block">
                                    <h5 itemprop="name">
                                       <a class="product-name" href="index.php?id_product=2&amp;controller=product&amp;id_lang=4" title="Glass-Nesting-Bowl-Set" itemprop="url">
                                       <span class="list-name">Glass-Nesting-Bowl-Set</span>
                                       <span class="grid-name">Glass-Nesting-Bowl-Set</span>
                                       </a>
                                    </h5>
                                    <p class="product-desc" itemprop="description">
                                       <span class="list-desc">Kitchen Supplies store was founded by several enthusiasts in 2002. Those were the times when people still preferred to buy products at brick-and-mortar stores instead of buying online.</span>
                                       <span class="grid-desc">Kitchen Supplies store was founded by...</span>
                                    </p>
                                    <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="content_price">
                                       <span itemprop="price" class="price product-price product-price-new">
                                       $16.00 </span>
                                       <meta itemprop="priceCurrency" content="USD">
                                       <span class="old-price product-price">
                                       $20.00
                                       </span>
                                       <span class="price-percent-reduction">-20%</span>
                                    </div>
                                    <div class="bottom_product_box">
                                       <div class="button-container">
                                          <a class="ajax_add_to_cart_button btn btn-default" href="index.php?controller=cart&amp;add=1&amp;id_product=2&amp;token=01ade8a68c5202cd5134eb506a06dd04" rel="nofollow" title="Añadir al carrito" data-id-product="2" data-minimal_quantity="1">
                                          <span>Añadir al carrito</span>
                                          </a>
                                          <a itemprop="url" class="lnk_view btn btn-default" href="index.php?id_product=2&amp;controller=product&amp;id_lang=4" title="Ver">
                                          <span>Más</span>
                                          </a>
                                       </div>
                                       <div class="comments_note" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                                          <div class="star_content clearfix">
                                             <div class="star star_on"></div>
                                             <div class="star star_on"></div>
                                             <div class="star star_on"></div>
                                             <div class="star"></div>
                                             <div class="star"></div>
                                             <meta itemprop="worstRating" content="0">
                                             <meta itemprop="ratingValue" content="3">
                                             <meta itemprop="bestRating" content="5">
                                          </div>
                                          <span class="nb-comments"><span itemprop="reviewCount">1</span> Comentario(s)</span>
                                       </div>
                                       <div class="product-flags"></div>
                                       <span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="availability">
                                          <span class="available-now">
                                             <link itemprop="availability" href="http://schema.org/InStock">
                                             En stock 
                                          </span>
                                       </span>
                                       <div class="more_qv">
                                          <div class="more_qv_buttons quick-view-box">
                                             <a class="quick-view" href="index.php?id_product=2&amp;controller=product&amp;id_lang=4" rel="index.php?id_product=2&amp;controller=product&amp;id_lang=4">
                                             <i class="fa fa-search"></i><span>Vista rápida</span>
                                             </a>
                                          </div>
                                          <div class="more_qv_buttons button_Box">
                                             <a itemprop="url" class="moreBox " href="index.php?id_product=2&amp;controller=product&amp;id_lang=4" title="Ver">
                                             <i class="fa fa-info"></i><span>Más</span> </a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li class="ajax_block_product col-xs-12 col-sm-4 col-md-3 last-item-of-tablet-line first-item-of-mobile-line bx-clone" style="float: left; list-style: none; position: relative; margin-right: 30px; opacity: 1; transform: translate3d(0px, 0px, 0px); width: 150.5px;">
                              <div class="product-container" itemscope="" itemtype="http://schema.org/Product">
                                 <div class="left-block">
                                    <div class="product-image-container">
                                       <a class="product_img_link" href="index.php?id_product=3&amp;controller=product&amp;id_lang=4" title="Holder-with-Tools-Set" itemprop="url">
                                       <img class="replace-2x img-responsive" src="img/250-tm_home_default.jpg" alt="Holder-with-Tools-Set" title="Holder-with-Tools-Set" itemprop="image">
                                       </a>
                                       <ul class="gallery-thumb-list">
                                          <li id="thumb-3-137" class="gallery-image-thumb">
                                             <a href="index.php?id_product=3&amp;controller=product&amp;id_lang=4" title="Holder-with-Tools-Set" data-href="img/137-tm_home_default.jpg">
                                             <img class="img-responsive" id="thumb-137" src="img/137-tm_cart_default.jpg" alt="Holder-with-Tools-Set" title="Holder-with-Tools-Set" itemprop="image">
                                             </a>
                                          </li>
                                          <li id="thumb-3-138" class="gallery-image-thumb">
                                             <a href="index.php?id_product=3&amp;controller=product&amp;id_lang=4" title="Holder-with-Tools-Set" data-href="img/138-tm_home_default.jpg">
                                             <img class="img-responsive" id="thumb-138" src="img/138-tm_cart_default.jpg" alt="Holder-with-Tools-Set" title="Holder-with-Tools-Set" itemprop="image">
                                             </a>
                                          </li>
                                          <li id="thumb-3-250" class="gallery-image-thumb active">
                                             <a href="index.php?id_product=3&amp;controller=product&amp;id_lang=4" title="Holder-with-Tools-Set" data-href="img/250-tm_home_default.jpg">
                                             <img class="img-responsive" id="thumb-250" src="img/250-tm_cart_default.jpg" alt="Holder-with-Tools-Set" title="Holder-with-Tools-Set" itemprop="image">
                                             </a>
                                          </li>
                                       </ul>
                                       <a class="sale-box" href="index.php?id_product=3&amp;controller=product&amp;id_lang=4">
                                       <span class="sale-label">¡Oferta!</span>
                                       </a>
                                    </div>
                                 </div>
                                 <div class="right-block">
                                    <h5 itemprop="name">
                                       <a class="product-name" href="index.php?id_product=3&amp;controller=product&amp;id_lang=4" title="Holder-with-Tools-Set" itemprop="url">
                                       <span class="list-name">Holder-with-Tools-Set</span>
                                       <span class="grid-name">Holder-with-Tools-Set</span>
                                       </a>
                                    </h5>
                                    <p class="product-desc" itemprop="description">
                                       <span class="list-desc">Kitchen Supplies store was founded by several enthusiasts in 2002. Those were the times when people still preferred to buy products at brick-and-mortar stores instead of buying online.</span>
                                       <span class="grid-desc">Kitchen Supplies store was founded by...</span>
                                    </p>
                                    <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="content_price">
                                       <span itemprop="price" class="price product-price">
                                       $90.00 </span>
                                       <meta itemprop="priceCurrency" content="USD">
                                    </div>
                                    <div class="bottom_product_box">
                                       <div class="button-container">
                                          <a class="ajax_add_to_cart_button btn btn-default" href="index.php?controller=cart&amp;add=1&amp;id_product=3&amp;token=01ade8a68c5202cd5134eb506a06dd04" rel="nofollow" title="Añadir al carrito" data-id-product="3" data-minimal_quantity="1">
                                          <span>Añadir al carrito</span>
                                          </a>
                                          <a itemprop="url" class="lnk_view btn btn-default" href="index.php?id_product=3&amp;controller=product&amp;id_lang=4" title="Ver">
                                          <span>Más</span>
                                          </a>
                                       </div>
                                       <div class="comments_note" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                                          <div class="star_content clearfix">
                                             <div class="star star_on"></div>
                                             <div class="star star_on"></div>
                                             <div class="star star_on"></div>
                                             <div class="star star_on"></div>
                                             <div class="star star_on"></div>
                                             <meta itemprop="worstRating" content="0">
                                             <meta itemprop="ratingValue" content="5">
                                             <meta itemprop="bestRating" content="5">
                                          </div>
                                          <span class="nb-comments"><span itemprop="reviewCount">1</span> Comentario(s)</span>
                                       </div>
                                       <div class="product-flags"></div>
                                       <span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="availability">
                                          <span class="available-now">
                                             <link itemprop="availability" href="http://schema.org/InStock">
                                             En stock 
                                          </span>
                                       </span>
                                       <div class="more_qv">
                                          <div class="more_qv_buttons quick-view-box">
                                             <a class="quick-view" href="index.php?id_product=3&amp;controller=product&amp;id_lang=4" rel="index.php?id_product=3&amp;controller=product&amp;id_lang=4">
                                             <i class="fa fa-search"></i><span>Vista rápida</span>
                                             </a>
                                          </div>
                                          <div class="more_qv_buttons button_Box">
                                             <a itemprop="url" class="moreBox " href="index.php?id_product=3&amp;controller=product&amp;id_lang=4" title="Ver">
                                             <i class="fa fa-info"></i><span>Más</span> </a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </li>

                           <?php foreach($articulo as $articulos){?>

                           <li class="ajax_block_product col-xs-12 col-sm-4 col-md-3 last-in-line first-item-of-tablet-line last-item-of-mobile-line bx-clone" style="float: left; list-style: none; position: relative; margin-right: 30px; opacity: 1; transform: translate3d(0px, 0px, 0px); width: 150.5px;">
                              <div class="product-container" itemscope="" itemtype="http://schema.org/Product">
                                 <div class="left-block">
                                    <div class="product-image-container">
                                       <a class="product_img_link" href="index.php?id_product=4&amp;controller=product&amp;id_lang=4" title="" itemprop="url">
                                       <img class="replace-2x img-responsive" src="img/253-tm_home_default.jpg" alt="" title="" itemprop="image">
                                       </a>
                                       <ul class="gallery-thumb-list">
                                          <li id="thumb-4-253" class="gallery-image-thumb active">
                                             <a href="index.php?id_product=4&amp;controller=product&amp;id_lang=4" title="" data-href="img/253-tm_home_default.jpg">
                                             <img class="img-responsive" id="thumb-253" src="img/253-tm_cart_default.jpg" alt="" title="" itemprop="image">
                                             </a>
                                          </li>
                                          <li id="thumb-4-254" class="gallery-image-thumb">
                                             <a href="index.php?id_product=4&amp;controller=product&amp;id_lang=4" title="" data-href="img/254-tm_home_default.jpg">
                                             <img class="img-responsive" id="thumb-254" src="img/254-tm_cart_default.jpg" alt="" title="" itemprop="image">
                                             </a>
                                          </li>
                                          <li id="thumb-4-255" class="gallery-image-thumb">
                                             <a href="index.php?id_product=4&amp;controller=product&amp;id_lang=4" title="" data-href="img/255-tm_home_default.jpg">
                                             <img class="img-responsive" id="thumb-255" src="img/255-tm_cart_default.jpg" alt="" title="" itemprop="image">
                                             </a>
                                          </li>
                                       </ul>
                                       <a class="sale-box" href="index.php?id_product=4&amp;controller=product&amp;id_lang=4">
                                       <span class="sale-label">¡Oferta!</span>
                                       </a>
                                    </div>
                                 </div>
                                 <div class="right-block">
                                    <h5 itemprop="name">
                                       <a class="product-name" href="index.php?id_product=4&amp;controller=product&amp;id_lang=4" title="" itemprop="url">
                                       <span class="list-name"></span>
                                       <span class="grid-name"><?php echo $articulos["Articulo"]["art_des"];?></span>
                                       </a>
                                    </h5>
                                    <p class="product-desc" itemprop="description">
                                       <span class="list-desc">Kitchen Supplies store was founded by several enthusiasts in 2002. Those were the times when people still preferred to buy products at brick-and-mortar stores instead of buying online.</span>
                                       <span class="grid-desc">Kitchen Supplies store was founded by...</span>
                                    </p>
                                    <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="content_price">
                                       <span itemprop="price" class="price product-price product-price-new">
                                       <?php echo $articulos["Articulo"]["precio"];?> </span>
                                       <meta itemprop="priceCurrency" content="USD">
                                       <!--<span class="old-price product-price">
                                       $15.00
                                       </span>
                                       <span class="price-percent-reduction">-20%</span>
                                       -->
                                    </div>
                                    <div class="bottom_product_box">
                                       <div class="button-container">
                                          <a class="ajax_add_to_cart_button btn btn-default" href="index.php?controller=cart&amp;add=1&amp;id_product=4&amp;token=01ade8a68c5202cd5134eb506a06dd04" rel="nofollow" title="Añadir al carrito" data-id-product="4" data-minimal_quantity="1">
                                          <span>Añadir al carrito</span>
                                          </a>
                                          <a itemprop="url" class="lnk_view btn btn-default" href="index.php?id_product=4&amp;controller=product&amp;id_lang=4" title="Ver">
                                          <span>Más</span>
                                          </a>
                                       </div>
                                       <div class="comments_note" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                                          <div class="star_content clearfix">
                                             <div class="star star_on"></div>
                                             <div class="star star_on"></div>
                                             <div class="star star_on"></div>
                                             <div class="star star_on"></div>
                                             <div class="star"></div>
                                             <meta itemprop="worstRating" content="0">
                                             <meta itemprop="ratingValue" content="4">
                                             <meta itemprop="bestRating" content="5">
                                          </div>
                                          <span class="nb-comments"><span itemprop="reviewCount">1</span> Comentario(s)</span>
                                       </div>
                                       <div class="product-flags"></div>
                                       <span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="availability">
                                          <span class="available-now">
                                             <link itemprop="availability" href="http://schema.org/InStock">
                                             En stock 
                                          </span>
                                       </span>
                                       <div class="more_qv">
                                          <div class="more_qv_buttons quick-view-box">
                                             <a class="quick-view" href="index.php?id_product=4&amp;controller=product&amp;id_lang=4" rel="index.php?id_product=4&amp;controller=product&amp;id_lang=4">
                                             <i class="fa fa-search"></i><span>Vista rápida</span>
                                             </a>
                                          </div>
                                          <div class="more_qv_buttons button_Box">
                                             <a itemprop="url" class="moreBox " href="index.php?id_product=4&amp;controller=product&amp;id_lang=4" title="Ver">
                                             <i class="fa fa-info"></i><span>Más</span> </a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <?php }?>

                        </ul>
                     </div>
                     <div class="bx-controls bx-has-controls-direction">
                        <div class="bx-controls-direction"><a class="bx-prev" href=""></a><a class="bx-next" href=""></a></div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- #center_column -->
         </div>
         <!-- .row -->
      </div>
      <!-- #columns -->
      <div class="bottom_content">
         <div id="htmlcontent_home">
            <div class="">
               <ul class="htmlcontent-home clearfix row">
                  <li class="htmlcontent-item-1 col-xs-3">
                     <a href="index.php?id_category=39&amp;controller=category" class="item-link" title="">
                        <div class="bannerBox">
                           <img src="img/ab0bdd53b26521af6f503488d027ea7c19b7d75e_banner-3.jpg" class="item-img" title="" alt="" width="100%" height="100%">
                           <div class="item-html">
                              <div>
                                 <div>
                                    <h3>Special Offer</h3>
                                    <h2>Lorem ipsum dolor<br>amet cons</h2>
                                    <button>Shop now!</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </a>
                  </li>
                  <li class="htmlcontent-item-2 col-xs-3">
                     <a href="index.php?id_category=40&amp;controller=category" class="item-link" title="">
                        <div class="bannerBox">
                           <img src="img/d992cb6864bca403fdf4ef713f5134b9a8e51b7d_banner-4.jpg" class="item-img" title="" alt="" width="100%" height="100%">
                           <div class="item-html">
                              <div>
                                 <div>
                                    <h3>Special Offer</h3>
                                    <h2>Lorem ipsum dolor<br>amet cons</h2>
                                    <button>Shop now!</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </a>
                  </li>
                  <li class="htmlcontent-item-3 col-xs-3">
                     <a href="index.php?id_category=41&amp;controller=category" class="item-link" title="">
                        <div class="bannerBox">
                           <img src="img/5569fa7efa534fa3704619c97551ea20694bf188_banner-5.jpg" class="item-img" title="" alt="" width="100%" height="100%">
                           <div class="item-html">
                              <div>
                                 <div>
                                    <h3>Special Offer</h3>
                                    <h2>Lorem ipsum dolor<br>amet cons</h2>
                                    <button>Shop now!</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </a>
                  </li>
                  <li class="htmlcontent-item-4 col-xs-3">
                     <a href="index.php?id_category=42&amp;controller=category" class="item-link" title="">
                        <div class="bannerBox">
                           <img src="img/f08f5046fa1085888af0abc700fd2c79fb30e002_banner-6.jpg" class="item-img" title="" alt="" width="100%" height="100%">
                           <div class="item-html">
                              <div>
                                 <div>
                                    <h3>Special Offer</h3>
                                    <h2>Lorem ipsum dolor<br>amet cons</h2>
                                    <button>Shop now!</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </a>
                  </li>
               </ul>
            </div>
         </div>
       <!--  <section id="homepage-blog" class="block">
            <div class="container">
               <h4 class="title_block"><a href="index.php?fc=module&amp;module=smartblog&amp;controller=category&amp;id_lang=4">Latest News</a></h4>
               <div class="block_content">
                  <div class="bx-wrapper" style="max-width: 1910px; margin: 0px auto;">
                     <div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: 218px;">
                        <ul id="homepage-carousel-blog" style="width: 615%; position: relative; left: -722px;">
                           <li class="bx-clone" style="float: left; list-style: none; position: relative; margin-right: 30px; width: 331px;">
                              <div class="blog-image">
                                 <a href="index.php?fc=module&amp;module=smartblog&amp;id_post=2&amp;controller=details&amp;id_lang=4"><img alt="Cras in sem in arcu ultrices" class="img-responsive" src="img/2-home-default.jpg"></a>
                              </div>
                              <span class="date-added">15 / 10 / 2014</span>
                              <h5><a class="product-name" href="index.php?fc=module&amp;module=smartblog&amp;id_post=2&amp;controller=details&amp;id_lang=4">Cras in sem in arcu ultrices</a></h5>
                              <p class="post-descr">
                                 Lorem ipsum dolor sit amet conse ctetur adipisic ing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
                              </p>
                              <div class="commentcount">
                                 <a href="index.php?fc=module&amp;module=smartblog&amp;id_post=2&amp;controller=details&amp;id_lang=4#commentInput"><i class="fa fa-comment"></i> 0</a>
                              </div>
                           </li>
                           <li class="bx-clone" style="float: left; list-style: none; position: relative; margin-right: 30px; width: 331px;">
                              <div class="blog-image">
                                 <a href="index.php?fc=module&amp;module=smartblog&amp;id_post=1&amp;controller=details&amp;id_lang=4"><img alt="Lorem ipsum dolor sit amet" class="img-responsive" src="img/1-home-default.jpg"></a>
                              </div>
                              <span class="date-added">15 / 10 / 2014</span>
                              <h5><a class="product-name" href="index.php?fc=module&amp;module=smartblog&amp;id_post=1&amp;controller=details&amp;id_lang=4">Lorem ipsum dolor sit amet</a></h5>
                              <p class="post-descr">
                                 Lorem ipsum dolor sit amet conse ctetur adipisic ing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
                              </p>
                              <div class="commentcount">
                                 <a href="index.php?fc=module&amp;module=smartblog&amp;id_post=1&amp;controller=details&amp;id_lang=4#commentInput"><i class="fa fa-comment"></i> 0</a>
                              </div>
                           </li>
                           <li class="" style="float: left; list-style: none; position: relative; margin-right: 30px; width: 331px;">
                              <div class="blog-image">
                                 <a href="index.php?fc=module&amp;module=smartblog&amp;id_post=4&amp;controller=details&amp;id_lang=4"><img alt="Lorem ipsum dolor amet cons" class="img-responsive" src="img/4-home-default.jpg"></a>
                              </div>
                              <span class="date-added">15 / 10 / 2014</span>
                              <h5><a class="product-name" href="index.php?fc=module&amp;module=smartblog&amp;id_post=4&amp;controller=details&amp;id_lang=4">Lorem ipsum dolor amet cons</a></h5>
                              <p class="post-descr">
                                 Lorem ipsum dolor sit amet conse ctetur adipisic ing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
                              </p>
                              <div class="commentcount">
                                 <a href="index.php?fc=module&amp;module=smartblog&amp;id_post=4&amp;controller=details&amp;id_lang=4#articleComments"><i class="fa fa-comment"></i> 5</a>
                              </div>
                           </li>
                           <li class="" style="float: left; list-style: none; position: relative; margin-right: 30px; width: 331px;">
                              <div class="blog-image">
                                 <a href="index.php?fc=module&amp;module=smartblog&amp;id_post=3&amp;controller=details&amp;id_lang=4"><img alt="Aliquam elementum lorem tristique" class="img-responsive" src="img/3-home-default.jpg"></a>
                              </div>
                              <span class="date-added">15 / 10 / 2014</span>
                              <h5><a class="product-name" href="index.php?fc=module&amp;module=smartblog&amp;id_post=3&amp;controller=details&amp;id_lang=4">Aliquam elementum lorem tristique</a></h5>
                              <p class="post-descr">
                                 Lorem ipsum dolor sit amet conse ctetur adipisic ing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
                              </p>
                              <div class="commentcount">
                                 <a href="index.php?fc=module&amp;module=smartblog&amp;id_post=3&amp;controller=details&amp;id_lang=4#commentInput"><i class="fa fa-comment"></i> 0</a>
                              </div>
                           </li>
                           <li class="" style="float: left; list-style: none; position: relative; margin-right: 30px; width: 331px;">
                              <div class="blog-image">
                                 <a href="index.php?fc=module&amp;module=smartblog&amp;id_post=2&amp;controller=details&amp;id_lang=4"><img alt="Cras in sem in arcu ultrices" class="img-responsive" src="img/2-home-default.jpg"></a>
                              </div>
                              <span class="date-added">15 / 10 / 2014</span>
                              <h5><a class="product-name" href="index.php?fc=module&amp;module=smartblog&amp;id_post=2&amp;controller=details&amp;id_lang=4">Cras in sem in arcu ultrices</a></h5>
                              <p class="post-descr">
                                 Lorem ipsum dolor sit amet conse ctetur adipisic ing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
                              </p>
                              <div class="commentcount">
                                 <a href="index.php?fc=module&amp;module=smartblog&amp;id_post=2&amp;controller=details&amp;id_lang=4#commentInput"><i class="fa fa-comment"></i> 0</a>
                              </div>
                           </li>
                           <li class="" style="float: left; list-style: none; position: relative; margin-right: 30px; width: 331px;">
                              <div class="blog-image">
                                 <a href="index.php?fc=module&amp;module=smartblog&amp;id_post=1&amp;controller=details&amp;id_lang=4"><img alt="Lorem ipsum dolor sit amet" class="img-responsive" src="img/1-home-default.jpg"></a>
                              </div>
                              <span class="date-added">15 / 10 / 2014</span>
                              <h5><a class="product-name" href="index.php?fc=module&amp;module=smartblog&amp;id_post=1&amp;controller=details&amp;id_lang=4">Lorem ipsum dolor sit amet</a></h5>
                              <p class="post-descr">
                                 Lorem ipsum dolor sit amet conse ctetur adipisic ing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
                              </p>
                              <div class="commentcount">
                                 <a href="index.php?fc=module&amp;module=smartblog&amp;id_post=1&amp;controller=details&amp;id_lang=4#commentInput"><i class="fa fa-comment"></i> 0</a>
                              </div>
                           </li>
                           <li class="bx-clone" style="float: left; list-style: none; position: relative; margin-right: 30px; width: 331px;">
                              <div class="blog-image">
                                 <a href="index.php?fc=module&amp;module=smartblog&amp;id_post=4&amp;controller=details&amp;id_lang=4"><img alt="Lorem ipsum dolor amet cons" class="img-responsive" src="img/4-home-default.jpg"></a>
                              </div>
                              <span class="date-added">15 / 10 / 2014</span>
                              <h5><a class="product-name" href="index.php?fc=module&amp;module=smartblog&amp;id_post=4&amp;controller=details&amp;id_lang=4">Lorem ipsum dolor amet cons</a></h5>
                              <p class="post-descr">
                                 Lorem ipsum dolor sit amet conse ctetur adipisic ing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
                              </p>
                              <div class="commentcount">
                                 <a href="index.php?fc=module&amp;module=smartblog&amp;id_post=4&amp;controller=details&amp;id_lang=4#articleComments"><i class="fa fa-comment"></i> 5</a>
                              </div>
                           </li>
                           <li class="bx-clone" style="float: left; list-style: none; position: relative; margin-right: 30px; width: 331px;">
                              <div class="blog-image">
                                 <a href="index.php?fc=module&amp;module=smartblog&amp;id_post=3&amp;controller=details&amp;id_lang=4"><img alt="Aliquam elementum lorem tristique" class="img-responsive" src="img/3-home-default.jpg"></a>
                              </div>
                              <span class="date-added">15 / 10 / 2014</span>
                              <h5><a class="product-name" href="index.php?fc=module&amp;module=smartblog&amp;id_post=3&amp;controller=details&amp;id_lang=4">Aliquam elementum lorem tristique</a></h5>
                              <p class="post-descr">
                                 Lorem ipsum dolor sit amet conse ctetur adipisic ing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
                              </p>
                              <div class="commentcount">
                                 <a href="index.php?fc=module&amp;module=smartblog&amp;id_post=3&amp;controller=details&amp;id_lang=4#commentInput"><i class="fa fa-comment"></i> 0</a>
                              </div>
                           </li>
                        </ul>
                     </div>
                     <div class="bx-controls bx-has-controls-direction">
                        <div class="bx-controls-direction"><a class="bx-prev" href=""></a><a class="bx-next" href=""></a></div>
                     </div>
                  </div>
               </div>
            </div>
         </section>-->
         <script>
            $(document).ready(function(){
            	if ($('#homepage-blog .block_content').width() < 940)
            		carousel_elements = 1;
            	if($('#homepage-blog .block_content').width() >= 940)
            		carousel_elements = 2;
            		
            	$('#homepage-carousel-blog > li').removeClass(),
            	
            	$('#homepage-carousel-blog').bxSlider({
            		responsive:true,
            		useCSS: false,
            		minSlides: carousel_elements,
            		maxSlides: carousel_elements,
            		slideWidth: 940,
            		slideMargin: 30,
            		moveSlides: 1,
            		pager: false,
            		autoHover: false,
            		speed: 500,
            		pause: 3000,
            		controls: true,
            		autoControls: true,
            		startText:'',
            		stopText:'',
            		prevText:'',
            		nextText:'',
            	});
            });
         </script>    
         <?php echo $this->element('manufactures'); ?>
      </div>
   </div>
   <!-- .columns-container -->
   <!-- Footer -->
   
   <!-- #footer -->
</div>
