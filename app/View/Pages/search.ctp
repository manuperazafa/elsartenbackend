<div class="columns-container">
<div id="columns" class="container">
 
<div class="breadcrumb clearfix">
<a class="home" href="http://livedemo00.template-help.com/prestashop_53429/" title="Volver a Inicio">
<i class="fa fa-home"></i>
</a>
<span class="navigation-pipe">&gt;</span>
Buscar
</div>
 
<div class="row">

<div id="left_column" class="column col-xs-12 col-sm-3"> 
<section id="best-sellers_block_right" class="block products_block">
<h4 class="title_block">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=best-sales" title="Ver los productos más vendidos">¡Lo más vendido!</a>
</h4>
<div class="block_content" style="">
<ul class="block_content products-block" style="">
<li class="clearfix">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4" title="Glass-Nesting-Bowl-Set" class="products-block-image content_img clearfix">
<img class="replace-2x img-responsive" src="http://livedemo00.template-help.com/prestashop_53429/img/p/2/4/4/244-tm_small_default.jpg" alt="Glass-Nesting-Bowl-Set">
</a>
<div class="product-content">
<h5>
<a class="product-name" href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4" title="Glass-Nesting-Bowl-Set">
Glass-Nesting-Bowl-Set
</a>
</h5>
<p class="product-description">Kitchen Supplies store was founded by several enthusiasts in 2002. Those...</p>
<div class="price-box">
<span class="price">$16.00</span>
</div>
</div>
</li>
<li class="clearfix">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=3&amp;controller=product&amp;id_lang=4" title="Holder-with-Tools-Set" class="products-block-image content_img clearfix">
<img class="replace-2x img-responsive" src="http://livedemo00.template-help.com/prestashop_53429/img/p/2/5/0/250-tm_small_default.jpg" alt="Holder-with-Tools-Set">
</a>
<div class="product-content">
<h5>
<a class="product-name" href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=3&amp;controller=product&amp;id_lang=4" title="Holder-with-Tools-Set">
Holder-with-Tools-Set
</a>
</h5>
<p class="product-description">Kitchen Supplies store was founded by several enthusiasts in 2002. Those...</p>
<div class="price-box">
<span class="price">$90.00</span>
</div>
</div>
</li>
<li class="clearfix">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=7&amp;controller=product&amp;id_lang=4" title="Double-Walled Ice Bucket" class="products-block-image content_img clearfix">
<img class="replace-2x img-responsive" src="http://livedemo00.template-help.com/prestashop_53429/img/p/2/7/3/273-tm_small_default.jpg" alt="Double-Walled Ice Bucket">
</a>
<div class="product-content">
<h5>
<a class="product-name" href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=7&amp;controller=product&amp;id_lang=4" title="Double-Walled Ice Bucket">
Double-Walled Ice Bucket
</a>
</h5>
<p class="product-description">Kitchen Supplies store was founded by several enthusiasts in 2002. Those...</p>
<div class="price-box">
<span class="price">$60.00</span>
</div>
</div>
</li>
<li class="clearfix">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=13&amp;controller=product&amp;id_lang=4" title="Bamboo-Flatware-Tray" class="products-block-image content_img clearfix">
<img class="replace-2x img-responsive" src="http://livedemo00.template-help.com/prestashop_53429/img/p/3/1/0/310-tm_small_default.jpg" alt="Bamboo-Flatware-Tray">
</a>
<div class="product-content">
<h5>
<a class="product-name" href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=13&amp;controller=product&amp;id_lang=4" title="Bamboo-Flatware-Tray">
Bamboo-Flatware-Tray
</a>
</h5>
<p class="product-description">Kitchen Supplies store was founded by several enthusiasts in 2002. Those...</p>
<div class="price-box">
<span class="price">$30.00</span>
</div>
</div>
</li>
<li class="clearfix">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=16&amp;controller=product&amp;id_lang=4" title="Rectangular-Cutting-Boards-with-Well" class="products-block-image content_img clearfix">
<img class="replace-2x img-responsive" src="http://livedemo00.template-help.com/prestashop_53429/img/p/3/2/8/328-tm_small_default.jpg" alt="Rectangular-Cutting-Boards-with-Well">
</a>
<div class="product-content">
<h5>
<a class="product-name" href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=16&amp;controller=product&amp;id_lang=4" title="Rectangular-Cutting-Boards-with-Well">
Rectangular-Cutting-Bo...
</a>
</h5>
<p class="product-description">Kitchen Supplies store was founded by several enthusiasts in 2002. Those...</p>
<div class="price-box">
<span class="price">$25.00</span>
</div>
</div>
</li>
</ul>
<div class="lnk">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=best-sales" title="Los productos más vendidos" class="btn btn-default btn-sm icon-right">
<span>
Los productos más vendidos
</span>
</a>
</div>
</div>
</section>
 <section id="wishlist_block" class="block account">
<h4 class="title_block">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?fc=module&amp;module=blockwishlist&amp;controller=mywishlist&amp;id_lang=4" title="Mi lista de regalos" rel="nofollow">
Lista de deseos
</a>
</h4>
<div class="block_content" style="">
<div id="wishlist_block_list" class="expanded">
<dl class="products no-products">
<dt>Ningún producto</dt>
<dd></dd>
</dl>
</div>  
<div class="lnk">
<a class="btn btn-default btn-sm icon-right" href="http://livedemo00.template-help.com/prestashop_53429/index.php?fc=module&amp;module=blockwishlist&amp;controller=mywishlist&amp;id_lang=4" title="Mi lista de regalos">
<span>
Mi lista de regalos
</span>
</a>
</div>  
</div>  
</section>   
<section id="special_block_right" class="block">
<h4 class="title_block">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=prices-drop" title="Promociones especiales">
Promociones especiales
</a>
</h4>
<div class="block_content products-block" style="">
<ul>
<li class="clearfix">
<a class="products-block-image" href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4">
<img class="replace-2x img-responsive" src="http://livedemo00.template-help.com/prestashop_53429/img/p/2/4/4/244-tm_small_default.jpg" alt="Glass-Nesting-Bowl-Set" title="Glass-Nesting-Bowl-Set">
</a>
<div class="product-content">
<h5>
<a class="product-name" href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4" title="Glass-Nesting-Bowl-Set">
Glass-Nesting-Bowl-Set
</a>
</h5>
<p class="product-description">
Kitchen Supplies store was founded by...
</p>
<div class="price-box">
<span class="price special-price">
$16.00
</span>
<span class="price-percent-reduction">-20%</span>
<span class="old-price">
$20.00
</span>
</div>
</div>
</li>
</ul>
<div>
<a class="btn btn-default btn-sm icon-right" href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=prices-drop" title="Todas los promociones especiales">
<span>
Todas los promociones especiales
</span>
</a>
</div>
</div>
</section>
  
<section id="suppliers_block_left" class="block blocksupplier">
<h4 class="title_block">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=supplier" title="Proveedores">
Proveedores
</a>
</h4>
<div class="block_content list-block" style="">
<ul>
<li class="first_item">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=3&amp;controller=supplier&amp;id_lang=4" title="Más sobre Consectetur adipiscing elit">
Consectetur adipiscing elit
</a>
</li>
<li class="item">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=1&amp;controller=supplier&amp;id_lang=4" title="Más sobre Curabitur ac ipsum pellentesque">
Curabitur ac ipsum pellentesque
</a>
</li>
<li class="item">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=6&amp;controller=supplier&amp;id_lang=4" title="Más sobre Interdum et malesuada">
Interdum et malesuada
</a>
</li>
<li class="item">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=2&amp;controller=supplier&amp;id_lang=4" title="Más sobre Lorem ipsum dolor sit amet">
Lorem ipsum dolor sit amet
</a>
</li>
<li class="item">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=7&amp;controller=supplier&amp;id_lang=4" title="Más sobre Morbi in volutpat eros">
Morbi in volutpat eros
</a>
</li>
</ul>
<form action="/prestashop_53429/index.php" method="get">
<div class="form-group selector1">
<div class="selector" style="width: 157px;"><span style="width: 145px; -webkit-user-select: none;">Todos los proveedores</span><select class="form-control" name="supplier_list">
<option value="0">Todos los proveedores</option>
<option value="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=3&amp;controller=supplier&amp;id_lang=4">Consectetur adipiscing elit</option>
<option value="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=1&amp;controller=supplier&amp;id_lang=4">Curabitur ac ipsum pellentesque</option>
<option value="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=6&amp;controller=supplier&amp;id_lang=4">Interdum et malesuada</option>
<option value="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=2&amp;controller=supplier&amp;id_lang=4">Lorem ipsum dolor sit amet</option>
<option value="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=7&amp;controller=supplier&amp;id_lang=4">Morbi in volutpat eros</option>
<option value="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=5&amp;controller=supplier&amp;id_lang=4">Nullam aliquam odio tellus</option>
<option value="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=8&amp;controller=supplier&amp;id_lang=4">Sed imperdiet porttitor nulla</option>
<option value="http://livedemo00.template-help.com/prestashop_53429/index.php?id_supplier=4&amp;controller=supplier&amp;id_lang=4">Ut feugiat lobortis</option>
</select></div>
</div>
</form>
</div>
</section>
 
 
<section id="viewed-products_block_left" class="block">
<h4 class="title_block">Productos más vistos</h4>
<div class="block_content products-block" style="">
<ul>
<li class="clearfix last_item">
<a class="products-block-image" href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4" title="Más acerca de Glass-Nesting-Bowl-Set">
<img class="img-responsive" src="http://livedemo00.template-help.com/prestashop_53429/img/p/2/4/4/244-tm_small_default.jpg" alt="Glass-Nesting-Bowl-Set">
</a>
<div class="product-content">
<h5>
<a class="product-name" href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4" title="Más acerca de Glass-Nesting-Bowl-Set">
Glass-Nesting-Bowl-Set
</a>
</h5>
<p class="product-description">Kitchen Supplies store was founded by...</p>
</div>
</li>
</ul>
</div>
</section>
 
<section id="informations_block_left_1" class="block informations_block_left">
<h4 class="title_block">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_cms_category=1&amp;controller=cms&amp;id_lang=4" title="Information">
Information </a>
</h4>
<div class="block_content list-block" style="">
<ul>
<li>
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_cms=1&amp;controller=cms&amp;id_lang=4" title="Delivery">
Delivery
</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_cms=2&amp;controller=cms&amp;id_lang=4" title="Legal Notice">
Legal Notice
</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_cms=3&amp;controller=cms&amp;id_lang=4" title="Terms and conditions of use">
Terms and conditions of use
</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_cms=4&amp;controller=cms&amp;id_lang=4" title="About us">
About us
</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_cms=5&amp;controller=cms&amp;id_lang=4" title="Secure payment">
Secure payment
</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=stores" title="Nuestras tiendas">
Nuestras tiendas
</a>
</li>
</ul>
</div>
</section>
 
 
<section id="stores_block_left" class="block">
<h4 class="title_block">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=stores" title="Nuestras tiendas">
Nuestras tiendas
</a>
</h4>
<div class="block_content blockstore" style="">
<p class="store_image">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=stores" title="Nuestras tiendas">
<img class="img-responsive" src="http://livedemo00.template-help.com/prestashop_53429/modules/blockstore/store.jpg" alt="Nuestras tiendas">
</a>
</p>
<div>
<a class="btn btn-default btn-sm icon-right" href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=stores" title="Nuestras tiendas">
<span>
Descubra nuestras tiendas
</span>
</a>
</div>
</div>
</section>
 
</div><div id="center_column" class="center_column col-xs-12 col-sm-9">
<h1 class="page-heading  product-listing">
Buscar&nbsp;
<span class="lighter">
"glass"
</span>
<span class="heading-counter">
1 resultado encontrado. </span>
</h1>
<div class="content_sortPagiBar">
<div class="sortPagiBar clearfix ">
<ul class="display hidden-xs">
<li class="display-title">Vista:</li>
<li id="grid" class="selected">
<a rel="nofollow" href="#" title="Cuadrícula">
<i class="fa fa-th-large"></i>
Cuadrícula
</a>
</li>
<li id="list">
<a rel="nofollow" href="#" title="Lista">
<i class="fa fa-th-list"></i>
Lista
</a>
</li>
</ul>
<form id="productsSortForm" action="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=search&amp;search_query=glass&amp;submit_search=" class="productsSortForm">
<div class="select selector1">
<label for="selectProductSort">Ordenar por</label>
<div class="selector" id="uniform-selectProductSort" style="width: 192px;"><span style="width: 180px; -webkit-user-select: none;">--</span><select id="selectProductSort" class="selectProductSort form-control">
<option value="name:asc">--</option>
<option value="price:asc">Precio: más baratos primero</option>
<option value="price:desc">Precio: más caros primero</option>
<option value="name:asc">Nombre: de A a Z</option>
<option value="name:desc">Nombre: de Z a A</option>
<option value="quantity:desc">En inventario primero</option>
<option value="reference:asc">Referencia: más bajo primero</option>
<option value="reference:desc">Referencia: más alto primero</option>
</select></div>
</div>
</form>
 
 
 
</div>
<div class="top-pagination-content clearfix">
<form method="post" action="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=products-comparison" class="compare-form">
<button type="submit" class="btn btn-default bt_compare icon-right" disabled="disabled">
<span>Comparar
(
<strong class="total-compare-val">0</strong>
)
</span>
</button>
<input type="hidden" name="compare_product_count" class="compare_product_count" value="0">
<input type="hidden" name="compare_product_list" class="compare_product_list" value="">
</form>
 
<div id="pagination" class="pagination clearfix">
</div>
<div class="product-count">
Mostrando 1 - 1 de 1 item
</div>
 
</div>
</div>
 
<ul class="product_list grid row">
<li class="ajax_block_product col-xs-12 col-sm-6 col-md-4 first-in-line last-line first-item-of-tablet-line first-item-of-mobile-line last-mobile-line" style="opacity: 1; transform: translate3d(0px, 0px, 0px);">
<div class="product-container" itemscope="" itemtype="http://schema.org/Product">
<div class="left-block">
<div class="product-image-container">
<a class="product_img_link" href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4&amp;search_query=glass&amp;results=1" title="Glass-Nesting-Bowl-Set" itemprop="url">
<img class="replace-2x img-responsive" src="http://livedemo00.template-help.com/prestashop_53429/img/p/2/4/4/244-tm_home_default.jpg" alt="Glass-Nesting-Bowl-Set" title="Glass-Nesting-Bowl-Set" itemprop="image">
</a>
<ul class="gallery-thumb-list">
<li id="thumb-2-244" class="gallery-image-thumb active">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4&amp;search_query=glass&amp;results=1" title="Glass-Nesting-Bowl-Set" data-href="http://livedemo00.template-help.com/prestashop_53429/img/p/2/4/4/244-tm_home_default.jpg">
<img class="img-responsive" id="thumb-244" src="http://livedemo00.template-help.com/prestashop_53429/img/p/2/4/4/244-tm_cart_default.jpg" alt="Glass-Nesting-Bowl-Set" title="Glass-Nesting-Bowl-Set" itemprop="image">
</a>
</li>
<li id="thumb-2-245" class="gallery-image-thumb">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4&amp;search_query=glass&amp;results=1" title="Glass-Nesting-Bowl-Set" data-href="http://livedemo00.template-help.com/prestashop_53429/img/p/2/4/5/245-tm_home_default.jpg">
<img class="img-responsive" id="thumb-245" src="http://livedemo00.template-help.com/prestashop_53429/img/p/2/4/5/245-tm_cart_default.jpg" alt="Glass-Nesting-Bowl-Set" title="Glass-Nesting-Bowl-Set" itemprop="image">
</a>
</li>
<li id="thumb-2-246" class="gallery-image-thumb">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4&amp;search_query=glass&amp;results=1" title="Glass-Nesting-Bowl-Set" data-href="http://livedemo00.template-help.com/prestashop_53429/img/p/2/4/6/246-tm_home_default.jpg">
<img class="img-responsive" id="thumb-246" src="http://livedemo00.template-help.com/prestashop_53429/img/p/2/4/6/246-tm_cart_default.jpg" alt="Glass-Nesting-Bowl-Set" title="Glass-Nesting-Bowl-Set" itemprop="image">
</a>
</li>
</ul>
<a class="new-box" href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4&amp;search_query=glass&amp;results=1">
<span class="new-label">Nuevo</span>
</a>
<a class="sale-box" href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4&amp;search_query=glass&amp;results=1">
<span class="sale-label">¡Oferta!</span>
</a>
</div>
</div>
<div class="right-block">
<h5 itemprop="name">
<a class="product-name" href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4&amp;search_query=glass&amp;results=1" title="Glass-Nesting-Bowl-Set" itemprop="url">
<span class="list-name">Glass-Nesting-Bowl-Set</span>
<span class="grid-name">Glass-Nesting-Bowl-Set</span>
</a>
</h5>
<p class="product-desc" itemprop="description">
<span class="list-desc">Kitchen Supplies store was founded by several enthusiasts in 2002. Those were the times when people still preferred to buy products at brick-and-mortar stores instead of buying online.</span>
<span class="grid-desc">Kitchen Supplies store was founded by...</span>
</p>
<div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="content_price">
<span itemprop="price" class="price product-price product-price-new">
$16.00 </span>
<meta itemprop="priceCurrency" content="USD">
<span class="old-price product-price">
$20.00
</span>
<span class="price-percent-reduction">-20%</span>
</div>
<div class="bottom_product_box">
<div class="button-container">
<a class="ajax_add_to_cart_button btn btn-default" href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=cart&amp;add=1&amp;id_product=2&amp;token=01ade8a68c5202cd5134eb506a06dd04" rel="nofollow" title="Añadir al carrito" data-id-product="2" data-minimal_quantity="1">
<span>Añadir al carrito</span>
</a>
<a itemprop="url" class="lnk_view btn btn-default" href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4&amp;search_query=glass&amp;results=1" title="Ver">
<span>Más</span>
</a>
</div>
<div class="comments_note" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
<div class="star_content clearfix">
<div class="star star_on"></div>
<div class="star star_on"></div>
<div class="star star_on"></div>
<div class="star"></div>
<div class="star"></div>
<meta itemprop="worstRating" content="0">
<meta itemprop="ratingValue" content="3">
<meta itemprop="bestRating" content="5">
</div>
<span class="nb-comments"><span itemprop="reviewCount">1</span> Comentario(s)</span>
</div>
<div class="color-list-container"><ul class="color_to_pick_list clearfix">
<li>
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4#/color-white/packege_quantity-5_piece" id="color_474" class="color_pick" style="background:#ffffff;">
</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4#/color-green/packege_quantity-5_piece" id="color_475" class="color_pick" style="background:#A0D468;">
</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4#/color-yellow/packege_quantity-5_piece" id="color_476" class="color_pick" style="background:#F1C40F;">
</a>
</li>
</ul></div>
<div class="product-flags">
</div>
<span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="availability">
<span class="available-now">
<link itemprop="availability" href="http://schema.org/InStock">En stock </span>
</span>
<div class="functional-buttons clearfix">
<div class="wishlist">
<a class="addToWishlist wishlistProd_2" href="#" rel="2" onclick="WishlistCart('wishlist_block_list', 'add', '2', false, 1); return false;" title="Añadir a la lista de deseos">
<span>Añadir a la lista de deseos</span>
</a>
</div>
<div class="compare">
<a class="add_to_compare" href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4&amp;search_query=glass&amp;results=1" data-id-product="2" title="Agregar para comparar"><span>Agregar para comparar</span></a>
</div>
<div class="more_qv_buttons">
<a class="quick-view" href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4&amp;search_query=glass&amp;results=1" rel="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4&amp;search_query=glass&amp;results=1">
<i class="fa fa-search"></i><span>Vista rápida</span>
</a>
</div>
<div class="buttonBox">
<a itemprop="url" class="moreBox " href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=2&amp;controller=product&amp;id_lang=4&amp;search_query=glass&amp;results=1" title="Ver">
<i class="fa fa-info"></i><span>Más</span> </a>
</div>
</div>
</div>
</div>
</div> 
</li>
</ul>
<div class="content_sortPagiBar">
<div class="bottom-pagination-content clearfix">
<form method="post" action="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=products-comparison" class="compare-form">
<button type="submit" class="btn btn-default bt_compare icon-right" disabled="disabled">
<span>Comparar
(
<strong class="total-compare-val">0</strong>
)
</span>
</button>
<input type="hidden" name="compare_product_count" class="compare_product_count" value="0">
<input type="hidden" name="compare_product_list" class="compare_product_list" value="">
</form>
 
<div id="pagination_bottom" class="pagination clearfix">
</div>
<div class="product-count">
Mostrando 1 - 1 de 1 item
</div>
 
</div>
</div>
</div> 
</div> 
</div> 
</div>