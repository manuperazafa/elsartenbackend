<div class="columns-container">
<div id="columns" class="container">
 
<div class="breadcrumb clearfix">
<a class="home" href="http://livedemo00.template-help.com/prestashop_53429/?live_configurator&amp;theme=theme4&amp;theme_font=" title="Volver a Inicio">
<i class="fa fa-home"></i>
</a>
<span class="navigation-pipe">&gt;</span>
Su carrito
</div>
 
<div class="row">
<div id="center_column" class="center_column col-xs-12 col-sm-12">
 
<h1 id="cart_title" class="page-heading">Productos en su carrito
<span class="heading-counter">Su carrito contiene
<span id="summary_products_quantity">1 producto</span>
</span>
</h1>
<p style="display:none" id="emptyCartWarning" class="alert alert-warning">Su carrito está vacío.</p>
<div class="cart_last_product">
<div class="cart_last_product_header">
<div class="left">Último producto añadido</div>
</div>
<a class="cart_last_product_img" href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=3&amp;controller=product&amp;id_lang=4&amp;live_configurator&amp;theme=theme4&amp;theme_font=">
<img src="http://livedemo00.template-help.com/prestashop_53429/img/p/2/5/0/250-tm_small_default.jpg" alt="Holder-with-Tools-Set">
</a>
<div class="cart_last_product_content">
<p class="product-name">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=3&amp;controller=product&amp;id_lang=4&amp;live_configurator&amp;theme=theme4&amp;theme_font=#/color-grey/type-kitchen_gadgets/packege_quantity-5_piece">
Holder-with-Tools-Set
</a>
</p>
<small>
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=3&amp;controller=product&amp;id_lang=4&amp;live_configurator&amp;theme=theme4&amp;theme_font=#/color-grey/type-kitchen_gadgets/packege_quantity-5_piece">
Color : Grey, Packege quantity : 5 Piece, Type : Kitchen gadgets
</a>
</small>
</div>
</div>
<div id="order-detail-content" class="table_block table-responsive">
<table id="cart_summary" class="table table-bordered stock-management-on">
<thead>
<tr>
<th class="cart_product first_item">producto</th>
<th class="cart_description item">Descripción</th>
<th class="cart_avail item">Avail.</th>
<th class="cart_unit item">Precio unitario</th>
<th class="cart_quantity item">Cant.</th>
<th class="cart_total item">Total</th>
<th class="cart_delete last_item">&nbsp;</th>
</tr>
</thead>
<tfoot>
<tr class="cart_total_price">
<td rowspan="4" colspan="2" id="cart_voucher" class="cart_voucher">
</td>
<td colspan="3" class="text-right">Total productos:</td>
<td colspan="2" class="price" id="total_product">$90.00</td>
</tr>
<tr style="display: none;">
<td colspan="3" class="text-right">
Total coste del envoltorio regalo </td>
<td colspan="2" class="price-discount price" id="total_wrapping">
$0.00
</td>
</tr>
<tr class="cart_total_delivery" style="">
<td colspan="3" class="text-right">Total envío:</td>
<td colspan="2" class="price" id="total_shipping">¡Envío gratuito!</td>
</tr>
<tr class="cart_total_voucher" style="display:none">
<td colspan="3" class="text-right">
Total cupones:
</td>
<td colspan="2" class="price-discount price" id="total_discount">
$0.00
</td>
</tr>
<tr class="cart_total_price">
<td colspan="3" class="total_price_container text-right">
<span>Total</span>
</td>
<td colspan="2" class="price" id="total_price_container">
<span id="total_price">$90.00</span>
</td>
</tr>
</tfoot>
<tbody>
<tr id="product_3_483_0_0" class="cart_item last_item first_item address_0 odd">
<td class="cart_product">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=3&amp;controller=product&amp;id_lang=4&amp;live_configurator&amp;theme=theme4&amp;theme_font=#/color-grey/type-kitchen_gadgets/packege_quantity-5_piece"><img src="http://livedemo00.template-help.com/prestashop_53429/img/p/2/5/0/250-tm_small_default.jpg" alt="Holder-with-Tools-Set"></a>
</td>
<td class="cart_description" data-title="Description">
<p class="product-name">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=3&amp;controller=product&amp;id_lang=4&amp;live_configurator&amp;theme=theme4&amp;theme_font=#/color-grey/type-kitchen_gadgets/packege_quantity-5_piece">Holder-with-Tools-Set</a>
</p>
<small class="cart_ref">SKU : 00103</small> <small>
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_product=3&amp;controller=product&amp;id_lang=4&amp;live_configurator&amp;theme=theme4&amp;theme_font=#/color-grey/type-kitchen_gadgets/packege_quantity-5_piece">Color : Grey, Packege quantity : 5 Piece, Type : Kitchen gadgets</a>
</small>
</td>
<td class="cart_avail"><span class="label label-success">En stock</span></td>
<td class="cart_unit" data-title="Precio unitario">
<span class="price" id="product_price_3_483_0">
<span class="price">$90.00</span>
</span>
</td>
<td class="cart_quantity" data-title="Qty">
<input type="hidden" value="1" name="quantity_3_483_0_0_hidden">
<input size="2" type="text" autocomplete="off" class="cart_quantity_input form-control grey" value="1" name="quantity_3_483_0_0">
<div class="cart_quantity_button clearfix">
<a rel="nofollow" class="cart_quantity_down btn btn-default button-minus" id="cart_quantity_down_3_483_0_0" href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=cart&amp;add=1&amp;id_product=3&amp;ipa=483&amp;id_address_delivery=0&amp;op=down&amp;token=01ade8a68c5202cd5134eb506a06dd04&amp;live_configurator&amp;theme=theme4&amp;theme_font=" title="Sustraer">
<span>
<i class="fa fa-minus"></i>
</span>
</a>
<a rel="nofollow" class="cart_quantity_up btn btn-default button-plus" id="cart_quantity_up_3_483_0_0" href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=cart&amp;add=1&amp;id_product=3&amp;ipa=483&amp;id_address_delivery=0&amp;token=01ade8a68c5202cd5134eb506a06dd04&amp;live_configurator&amp;theme=theme4&amp;theme_font=" title="Añadir">
<span>
<i class="fa fa-plus"></i>
</span>
</a>
</div>
</td>
<td class="cart_total" data-title="Total">
<span class="price" id="total_product_price_3_483_0">
$90.00 </span>
</td>
<td class="cart_delete text-center">
<div>
<a rel="nofollow" title="Eliminar" class="cart_quantity_delete" id="3_483_0_0" href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=cart&amp;delete=1&amp;id_product=3&amp;ipa=483&amp;id_address_delivery=0&amp;token=01ade8a68c5202cd5134eb506a06dd04&amp;live_configurator&amp;theme=theme4&amp;theme_font="><i class="fa fa-trash-o"></i></a>
</div>
</td>
</tr>
</tbody>
</table>
</div>  
<div id="HOOK_SHOPPING_CART"></div>
<p class="cart_navigation clearfix">
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=order-opc&amp;live_configurator&amp;theme=theme5&amp;live_configurator&amp;theme=theme4&amp;theme_font=" class="btn btn-default icon-left" title="Continuar la compra">
<span>Continuar la compra</span>
</a>
</p>
 
 
<div id="opc_new_account" class="opc-main-block">
<div id="opc_new_account-overlay" class="opc-overlay" style="display: none;"></div>
<h1 class="page-heading step-num"><span>1</span> Cuenta</h1>
<form action="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=authentication&amp;back=order-opc" method="post" id="login_form" class="box">
<fieldset>
<h3 class="page-subheading">¿Ya está registrado?</h3>
<p>
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=authentication&amp;live_configurator&amp;theme=theme4&amp;theme_font=" id="openLoginFormBlock">» Pulse aquí</a>
</p>
<div id="login_form_content" style="display:none;">
 
<div id="opc_login_errors" class="alert alert-danger" style="display:none;"></div>
 
<p class="form-group">
<label for="login_email">Correo electrónico</label>
<input type="text" class="form-control validate" id="login_email" name="email" data-validate="isEmail">
</p>
<p class="form-group">
<label for="login_passwd">Contraseña</label>
<input class="form-control validate" type="password" id="login_passwd" name="login_passwd" data-validate="isPasswd">
</p>
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=password&amp;live_configurator&amp;theme=theme4&amp;theme_font=" class="lost_password" title="¿Olvidó su contraseña?">¿Olvidó su contraseña?</a>
<p class="submit">
<input type="hidden" class="hidden" name="back" value="">
<button type="submit" id="SubmitLogin" name="SubmitLogin" class="btn btn-default btn-md">
<span>
<i class="fa fa-lock left"></i>
Iniciar sesión
</span>
</button>
</p>
</div>
</fieldset>
</form>
<form action="http://livedemo00.template-help.com/prestashop_53429/index.php?controller=authentication" method="post" id="new_account_form" class="std" autocomplete="on" autofill="on">
<fieldset>
<div class="box">
<h3 id="new_account_title" class="page-subheading">Nuevo cliente</h3>
<div id="opc_account_choice" class="row">
<div class="col-xs-12 col-md-6">
<p class="title_block">Pedido instantáneo</p>
<p class="opc-button">
<button type="submit" class="btn btn-default btn-md" id="opc_guestCheckout">
<span>
Hacer un pedido como invitado
</span>
</button>
</p>
</div>
<div class="col-xs-12 col-md-6">
<p class="title_block">Cree su cuenta hoy mismo y aproveche</p>
<ul class="bullet">
<li>- Acceso seguro y personalizado</li>
<li>- Pedido fácil y rápido</li>
<li>- Diferentes direcciones para el envío y la facturación disponible</li>
</ul>
<p class="opc-button">
<button type="submit" class="btn btn-default btn-md" id="opc_createAccount">
<span>
<i class="fa fa-user left"></i>
Crear una cuenta
</span>
</button>
</p>
</div>
</div>
<div id="opc_account_form" class="unvisible" style="display: none;">
 
<div id="opc_account_errors" class="alert alert-danger" style="display:none;"></div>
 
 
<input type="hidden" id="is_new_customer" name="is_new_customer" value="0">
<input type="hidden" id="opc_id_customer" name="opc_id_customer" value="0">
<input type="hidden" id="opc_id_address_delivery" name="opc_id_address_delivery" value="0">
<input type="hidden" id="opc_id_address_invoice" name="opc_id_address_invoice" value="0">
<div class="required text form-group">
<label for="email">Correo electrónico <sup>*</sup></label>
<input type="text" class="text form-control validate" id="email" name="email" data-validate="isEmail" value="">
</div>
<div class="required password is_customer_param form-group">
<label for="passwd">Contraseña <sup>*</sup></label>
<input type="password" class="text form-control validate" name="passwd" id="passwd" data-validate="isPasswd">
<span class="form_info">(Mínimo 5 caracteres)</span>
</div>
<div class="required clearfix gender-line">
<label>Tratamiento</label>
<div class="radio-inline">
<label for="id_gender1" class="top">
<div class="radio" id="uniform-id_gender1"><span><input type="radio" name="id_gender" id="id_gender1" value="1"></span></div>
Mr.
</label>
</div>
<div class="radio-inline">
<label for="id_gender2" class="top">
<div class="radio" id="uniform-id_gender2"><span><input type="radio" name="id_gender" id="id_gender2" value="2"></span></div>
Mrs.
</label>
</div>
</div>
<div class="required form-group">
<label for="firstname">Nombre <sup>*</sup></label>
<input type="text" class="text form-control validate" id="customer_firstname" name="customer_firstname" onblur="$('#firstname').val($(this).val());" data-validate="isName" value="">
</div>
<div class="required form-group">
<label for="lastname">Apellido <sup>*</sup></label>
<input type="text" class="form-control validate" id="customer_lastname" name="customer_lastname" onblur="$('#lastname').val($(this).val());" data-validate="isName" value="">
</div>
<div class="select form-group date-select">
<label>Fecha de nacimiento</label>
<div class="row">
<div class="col-xs-4">
<div class="selector" id="uniform-days" style="width: 33px;"><span style="width: 21px; -webkit-user-select: none;">-</span><select id="days" name="days" class="form-control">
<option value="">-</option>
<option value="1">1&nbsp;&nbsp;</option>
<option value="2">2&nbsp;&nbsp;</option>
<option value="3">3&nbsp;&nbsp;</option>
<option value="4">4&nbsp;&nbsp;</option>
<option value="5">5&nbsp;&nbsp;</option>
<option value="6">6&nbsp;&nbsp;</option>
<option value="7">7&nbsp;&nbsp;</option>
<option value="8">8&nbsp;&nbsp;</option>
<option value="9">9&nbsp;&nbsp;</option>
<option value="10">10&nbsp;&nbsp;</option>
<option value="11">11&nbsp;&nbsp;</option>
<option value="12">12&nbsp;&nbsp;</option>
<option value="13">13&nbsp;&nbsp;</option>
<option value="14">14&nbsp;&nbsp;</option>
<option value="15">15&nbsp;&nbsp;</option>
<option value="16">16&nbsp;&nbsp;</option>
<option value="17">17&nbsp;&nbsp;</option>
<option value="18">18&nbsp;&nbsp;</option>
<option value="19">19&nbsp;&nbsp;</option>
<option value="20">20&nbsp;&nbsp;</option>
<option value="21">21&nbsp;&nbsp;</option>
<option value="22">22&nbsp;&nbsp;</option>
<option value="23">23&nbsp;&nbsp;</option>
<option value="24">24&nbsp;&nbsp;</option>
<option value="25">25&nbsp;&nbsp;</option>
<option value="26">26&nbsp;&nbsp;</option>
<option value="27">27&nbsp;&nbsp;</option>
<option value="28">28&nbsp;&nbsp;</option>
<option value="29">29&nbsp;&nbsp;</option>
<option value="30">30&nbsp;&nbsp;</option>
<option value="31">31&nbsp;&nbsp;</option>
</select></div>
</div>
<div class="col-xs-4">
<div class="selector" id="uniform-months" style="width: 30px;"><span style="width: 18px; -webkit-user-select: none;">-</span><select id="months" name="months" class="form-control">
<option value="">-</option>
<option value="1">Enero&nbsp;</option>
<option value="2">Febrero&nbsp;</option>
<option value="3">Marzo&nbsp;</option>
<option value="4">Abril&nbsp;</option>
<option value="5">Mayo&nbsp;</option>
<option value="6">Junio&nbsp;</option>
<option value="7">Julio&nbsp;</option>
<option value="8">Agosto&nbsp;</option>
<option value="9">Septiembre&nbsp;</option>
<option value="10">Octubre&nbsp;</option>
<option value="11">Noviembre&nbsp;</option>
<option value="12">Diciembre&nbsp;</option>
</select></div>
</div>
<div class="col-xs-4">
<div class="selector" id="uniform-years" style="width: 28px;"><span style="width: 16px; -webkit-user-select: none;">-</span><select id="years" name="years" class="form-control">
<option value="">-</option>
<option value="2016">2016&nbsp;&nbsp;</option>
<option value="2015">2015&nbsp;&nbsp;</option>
<option value="2014">2014&nbsp;&nbsp;</option>
<option value="2013">2013&nbsp;&nbsp;</option>
<option value="2012">2012&nbsp;&nbsp;</option>
<option value="2011">2011&nbsp;&nbsp;</option>
<option value="2010">2010&nbsp;&nbsp;</option>
<option value="2009">2009&nbsp;&nbsp;</option>
<option value="2008">2008&nbsp;&nbsp;</option>
<option value="2007">2007&nbsp;&nbsp;</option>
<option value="2006">2006&nbsp;&nbsp;</option>
<option value="2005">2005&nbsp;&nbsp;</option>
<option value="2004">2004&nbsp;&nbsp;</option>
<option value="2003">2003&nbsp;&nbsp;</option>
<option value="2002">2002&nbsp;&nbsp;</option>
<option value="2001">2001&nbsp;&nbsp;</option>
<option value="2000">2000&nbsp;&nbsp;</option>
<option value="1999">1999&nbsp;&nbsp;</option>
<option value="1998">1998&nbsp;&nbsp;</option>
<option value="1997">1997&nbsp;&nbsp;</option>
<option value="1996">1996&nbsp;&nbsp;</option>
<option value="1995">1995&nbsp;&nbsp;</option>
<option value="1994">1994&nbsp;&nbsp;</option>
<option value="1993">1993&nbsp;&nbsp;</option>
<option value="1992">1992&nbsp;&nbsp;</option>
<option value="1991">1991&nbsp;&nbsp;</option>
<option value="1990">1990&nbsp;&nbsp;</option>
<option value="1989">1989&nbsp;&nbsp;</option>
<option value="1988">1988&nbsp;&nbsp;</option>
<option value="1987">1987&nbsp;&nbsp;</option>
<option value="1986">1986&nbsp;&nbsp;</option>
<option value="1985">1985&nbsp;&nbsp;</option>
<option value="1984">1984&nbsp;&nbsp;</option>
<option value="1983">1983&nbsp;&nbsp;</option>
<option value="1982">1982&nbsp;&nbsp;</option>
<option value="1981">1981&nbsp;&nbsp;</option>
<option value="1980">1980&nbsp;&nbsp;</option>
<option value="1979">1979&nbsp;&nbsp;</option>
<option value="1978">1978&nbsp;&nbsp;</option>
<option value="1977">1977&nbsp;&nbsp;</option>
<option value="1976">1976&nbsp;&nbsp;</option>
<option value="1975">1975&nbsp;&nbsp;</option>
<option value="1974">1974&nbsp;&nbsp;</option>
<option value="1973">1973&nbsp;&nbsp;</option>
<option value="1972">1972&nbsp;&nbsp;</option>
<option value="1971">1971&nbsp;&nbsp;</option>
<option value="1970">1970&nbsp;&nbsp;</option>
<option value="1969">1969&nbsp;&nbsp;</option>
<option value="1968">1968&nbsp;&nbsp;</option>
<option value="1967">1967&nbsp;&nbsp;</option>
<option value="1966">1966&nbsp;&nbsp;</option>
<option value="1965">1965&nbsp;&nbsp;</option>
<option value="1964">1964&nbsp;&nbsp;</option>
<option value="1963">1963&nbsp;&nbsp;</option>
<option value="1962">1962&nbsp;&nbsp;</option>
<option value="1961">1961&nbsp;&nbsp;</option>
<option value="1960">1960&nbsp;&nbsp;</option>
<option value="1959">1959&nbsp;&nbsp;</option>
<option value="1958">1958&nbsp;&nbsp;</option>
<option value="1957">1957&nbsp;&nbsp;</option>
<option value="1956">1956&nbsp;&nbsp;</option>
<option value="1955">1955&nbsp;&nbsp;</option>
<option value="1954">1954&nbsp;&nbsp;</option>
<option value="1953">1953&nbsp;&nbsp;</option>
<option value="1952">1952&nbsp;&nbsp;</option>
<option value="1951">1951&nbsp;&nbsp;</option>
<option value="1950">1950&nbsp;&nbsp;</option>
<option value="1949">1949&nbsp;&nbsp;</option>
<option value="1948">1948&nbsp;&nbsp;</option>
<option value="1947">1947&nbsp;&nbsp;</option>
<option value="1946">1946&nbsp;&nbsp;</option>
<option value="1945">1945&nbsp;&nbsp;</option>
<option value="1944">1944&nbsp;&nbsp;</option>
<option value="1943">1943&nbsp;&nbsp;</option>
<option value="1942">1942&nbsp;&nbsp;</option>
<option value="1941">1941&nbsp;&nbsp;</option>
<option value="1940">1940&nbsp;&nbsp;</option>
<option value="1939">1939&nbsp;&nbsp;</option>
<option value="1938">1938&nbsp;&nbsp;</option>
<option value="1937">1937&nbsp;&nbsp;</option>
<option value="1936">1936&nbsp;&nbsp;</option>
<option value="1935">1935&nbsp;&nbsp;</option>
<option value="1934">1934&nbsp;&nbsp;</option>
<option value="1933">1933&nbsp;&nbsp;</option>
<option value="1932">1932&nbsp;&nbsp;</option>
<option value="1931">1931&nbsp;&nbsp;</option>
<option value="1930">1930&nbsp;&nbsp;</option>
<option value="1929">1929&nbsp;&nbsp;</option>
<option value="1928">1928&nbsp;&nbsp;</option>
<option value="1927">1927&nbsp;&nbsp;</option>
<option value="1926">1926&nbsp;&nbsp;</option>
<option value="1925">1925&nbsp;&nbsp;</option>
<option value="1924">1924&nbsp;&nbsp;</option>
<option value="1923">1923&nbsp;&nbsp;</option>
<option value="1922">1922&nbsp;&nbsp;</option>
<option value="1921">1921&nbsp;&nbsp;</option>
<option value="1920">1920&nbsp;&nbsp;</option>
<option value="1919">1919&nbsp;&nbsp;</option>
<option value="1918">1918&nbsp;&nbsp;</option>
<option value="1917">1917&nbsp;&nbsp;</option>
<option value="1916">1916&nbsp;&nbsp;</option>
<option value="1915">1915&nbsp;&nbsp;</option>
<option value="1914">1914&nbsp;&nbsp;</option>
<option value="1913">1913&nbsp;&nbsp;</option>
<option value="1912">1912&nbsp;&nbsp;</option>
<option value="1911">1911&nbsp;&nbsp;</option>
<option value="1910">1910&nbsp;&nbsp;</option>
<option value="1909">1909&nbsp;&nbsp;</option>
<option value="1908">1908&nbsp;&nbsp;</option>
<option value="1907">1907&nbsp;&nbsp;</option>
<option value="1906">1906&nbsp;&nbsp;</option>
<option value="1905">1905&nbsp;&nbsp;</option>
<option value="1904">1904&nbsp;&nbsp;</option>
<option value="1903">1903&nbsp;&nbsp;</option>
<option value="1902">1902&nbsp;&nbsp;</option>
<option value="1901">1901&nbsp;&nbsp;</option>
<option value="1900">1900&nbsp;&nbsp;</option>
</select></div>
</div>
</div>
</div>
<div class="checkbox">
<label for="newsletter">
<div class="checker" id="uniform-newsletter"><span><input type="checkbox" name="newsletter" id="newsletter" value="1" autocomplete="off"></span></div>
Inscríbase a nuestra lista de correo
</label>
</div>
<div class="checkbox">
<label for="optin">
<div class="checker" id="uniform-optin"><span><input type="checkbox" name="optin" id="optin" value="1" autocomplete="off"></span></div>
Recíba ofertas especiales de nuestros socios
</label>
</div>
<h3 class="page-subheading top-indent">Dirección de entrega</h3>
<div class="required text form-group">
<label for="firstname">Nombre <sup>*</sup></label>
<input type="text" class="text form-control validate" id="firstname" name="firstname" data-validate="isName" value="">
</div>
<div class="required text form-group">
<label for="lastname">Apellido <sup>*</sup></label>
<input type="text" class="text form-control validate" id="lastname" name="lastname" data-validate="isName" value="">
</div>
<div class="text form-group">
<label for="company">Empresa</label>
<input type="text" class="text form-control validate" id="company" name="company" data-validate="isName" value="">
</div>
<div class="required text form-group">
<label for="address1">Dirección <sup>*</sup></label>
<input type="text" class="text form-control validate" name="address1" id="address1" data-validate="isAddress" value="">
</div>
<div class="text is_customer_param form-group">
<label for="address2">Dirección (2)</label>
<input type="text" class="text form-control validate" name="address2" id="address2" data-validate="isAddress" value="">
</div>
<div class="required text form-group">
<label for="city">Ciudad <sup>*</sup></label>
<input type="text" class="text form-control validate" name="city" id="city" data-validate="isCityName" value="">
</div>
<div class="required id_state form-group" style="display: none;">
<label for="id_state">Estado <sup>*</sup></label>
<div class="selector" id="uniform-id_state" style="width: 48px;"><span style="width: 36px; -webkit-user-select: none;">-</span><select name="id_state" id="id_state" class="form-control">
<option value="">-</option>
</select></div>
</div>
<div class="required postcode text form-group" style="display: block;">
<label for="postcode">Código postal <sup>*</sup></label>
<input type="text" class="text form-control validate uniform-input" name="postcode" id="postcode" data-validate="isPostCode" value="" onkeyup="$('#postcode').val($('#postcode').val().toUpperCase());">
</div>
<div class="required select form-group">
<label for="id_country">País <sup>*</sup></label>
<div class="selector" id="uniform-id_country" style="width: 31px;"><span style="width: 19px; -webkit-user-select: none;">Spain</span><select name="id_country" id="id_country" class="form-control">
<option value="231">Afghanistan</option>
<option value="244">Åland Islands</option>
<option value="230">Albania</option>
<option value="38">Algeria</option>
<option value="39">American Samoa</option>
<option value="40">Andorra</option>
<option value="41">Angola</option>
<option value="42">Anguilla</option>
<option value="232">Antarctica</option>
<option value="43">Antigua and Barbuda</option>
<option value="44">Argentina</option>
<option value="45">Armenia</option>
<option value="46">Aruba</option>
<option value="24">Australia</option>
<option value="2">Austria</option>
<option value="47">Azerbaijan</option>
<option value="48">Bahamas</option>
<option value="49">Bahrain</option>
<option value="50">Bangladesh</option>
<option value="51">Barbados</option>
<option value="52">Belarus</option>
<option value="3">Belgium</option>
<option value="53">Belize</option>
<option value="54">Benin</option>
<option value="55">Bermuda</option>
<option value="56">Bhutan</option>
<option value="34">Bolivia</option>
<option value="233">Bosnia and Herzegovina</option>
<option value="57">Botswana</option>
<option value="234">Bouvet Island</option>
<option value="58">Brazil</option>
<option value="235">British Indian Ocean Territory</option>
<option value="59">Brunei</option>
<option value="236">Bulgaria</option>
<option value="60">Burkina Faso</option>
<option value="61">Burma (Myanmar)</option>
<option value="62">Burundi</option>
<option value="63">Cambodia</option>
<option value="64">Cameroon</option>
<option value="4">Canada</option>
<option value="65">Cape Verde</option>
<option value="237">Cayman Islands</option>
<option value="66">Central African Republic</option>
<option value="67">Chad</option>
<option value="68">Chile</option>
<option value="5">China</option>
<option value="238">Christmas Island</option>
<option value="239">Cocos (Keeling) Islands</option>
<option value="69">Colombia</option>
<option value="70">Comoros</option>
<option value="71">Congo, Dem. Republic</option>
<option value="72">Congo, Republic</option>
<option value="240">Cook Islands</option>
<option value="73">Costa Rica</option>
<option value="74">Croatia</option>
<option value="75">Cuba</option>
<option value="76">Cyprus</option>
<option value="16">Czech Republic</option>
<option value="20">Denmark</option>
<option value="77">Djibouti</option>
<option value="78">Dominica</option>
<option value="79">Dominican Republic</option>
<option value="80">East Timor</option>
<option value="81">Ecuador</option>
<option value="82">Egypt</option>
<option value="83">El Salvador</option>
<option value="84">Equatorial Guinea</option>
<option value="85">Eritrea</option>
<option value="86">Estonia</option>
<option value="87">Ethiopia</option>
<option value="88">Falkland Islands</option>
<option value="89">Faroe Islands</option>
<option value="90">Fiji</option>
<option value="7">Finland</option>
<option value="8">France</option>
<option value="241">French Guiana</option>
<option value="242">French Polynesia</option>
<option value="243">French Southern Territories</option>
<option value="91">Gabon</option>
<option value="92">Gambia</option>
<option value="93">Georgia</option>
<option value="1">Germany</option>
<option value="94">Ghana</option>
<option value="97">Gibraltar</option>
<option value="9">Greece</option>
<option value="96">Greenland</option>
<option value="95">Grenada</option>
<option value="98">Guadeloupe</option>
<option value="99">Guam</option>
<option value="100">Guatemala</option>
<option value="101">Guernsey</option>
<option value="102">Guinea</option>
<option value="103">Guinea-Bissau</option>
<option value="104">Guyana</option>
<option value="105">Haiti</option>
<option value="106">Heard Island and McDonald Islands</option>
<option value="108">Honduras</option>
<option value="22">HongKong</option>
<option value="143">Hungary</option>
<option value="109">Iceland</option>
<option value="110">India</option>
<option value="111">Indonesia</option>
<option value="112">Iran</option>
<option value="113">Iraq</option>
<option value="26">Ireland</option>
<option value="29">Israel</option>
<option value="10">Italy</option>
<option value="32">Ivory Coast</option>
<option value="115">Jamaica</option>
<option value="11">Japan</option>
<option value="116">Jersey</option>
<option value="117">Jordan</option>
<option value="118">Kazakhstan</option>
<option value="119">Kenya</option>
<option value="120">Kiribati</option>
<option value="121">Korea, Dem. Republic of</option>
<option value="122">Kuwait</option>
<option value="123">Kyrgyzstan</option>
<option value="124">Laos</option>
<option value="125">Latvia</option>
<option value="126">Lebanon</option>
<option value="127">Lesotho</option>
<option value="128">Liberia</option>
<option value="129">Libya</option>
<option value="130">Liechtenstein</option>
<option value="131">Lithuania</option>
<option value="12">Luxemburg</option>
<option value="132">Macau</option>
<option value="133">Macedonia</option>
<option value="134">Madagascar</option>
<option value="135">Malawi</option>
<option value="136">Malaysia</option>
<option value="137">Maldives</option>
<option value="138">Mali</option>
<option value="139">Malta</option>
<option value="114">Man Island</option>
<option value="140">Marshall Islands</option>
<option value="141">Martinique</option>
<option value="142">Mauritania</option>
<option value="35">Mauritius</option>
<option value="144">Mayotte</option>
<option value="145">Mexico</option>
<option value="146">Micronesia</option>
<option value="147">Moldova</option>
<option value="148">Monaco</option>
<option value="149">Mongolia</option>
<option value="150">Montenegro</option>
<option value="151">Montserrat</option>
<option value="152">Morocco</option>
<option value="153">Mozambique</option>
<option value="154">Namibia</option>
<option value="155">Nauru</option>
<option value="156">Nepal</option>
<option value="13">Netherlands</option>
<option value="157">Netherlands Antilles</option>
<option value="158">New Caledonia</option>
<option value="27">New Zealand</option>
<option value="159">Nicaragua</option>
<option value="160">Niger</option>
<option value="31">Nigeria</option>
<option value="161">Niue</option>
<option value="162">Norfolk Island</option>
<option value="163">Northern Mariana Islands</option>
<option value="23">Norway</option>
<option value="164">Oman</option>
<option value="165">Pakistan</option>
<option value="166">Palau</option>
<option value="167">Palestinian Territories</option>
<option value="168">Panama</option>
<option value="169">Papua New Guinea</option>
<option value="170">Paraguay</option>
<option value="171">Peru</option>
<option value="172">Philippines</option>
<option value="173">Pitcairn</option>
<option value="14">Poland</option>
<option value="15">Portugal</option>
<option value="174">Puerto Rico</option>
<option value="175">Qatar</option>
<option value="176">Reunion Island</option>
<option value="36">Romania</option>
<option value="177">Russian Federation</option>
<option value="178">Rwanda</option>
<option value="179">Saint Barthelemy</option>
<option value="180">Saint Kitts and Nevis</option>
<option value="181">Saint Lucia</option>
<option value="182">Saint Martin</option>
<option value="183">Saint Pierre and Miquelon</option>
<option value="184">Saint Vincent and the Grenadines</option>
<option value="185">Samoa</option>
<option value="186">San Marino</option>
<option value="187">São Tomé and Príncipe</option>
<option value="188">Saudi Arabia</option>
<option value="189">Senegal</option>
<option value="190">Serbia</option>
<option value="191">Seychelles</option>
<option value="192">Sierra Leone</option>
<option value="25">Singapore</option>
<option value="37">Slovakia</option>
<option value="193">Slovenia</option>
<option value="194">Solomon Islands</option>
<option value="195">Somalia</option>
<option value="30">South Africa</option>
<option value="196">South Georgia and the South Sandwich Islands</option>
<option value="28">South Korea</option>
<option value="6" selected="selected">Spain</option>
<option value="197">Sri Lanka</option>
<option value="198">Sudan</option>
<option value="199">Suriname</option>
<option value="200">Svalbard and Jan Mayen</option>
<option value="201">Swaziland</option>
<option value="18">Sweden</option>
<option value="19">Switzerland</option>
<option value="202">Syria</option>
<option value="203">Taiwan</option>
<option value="204">Tajikistan</option>
<option value="205">Tanzania</option>
<option value="206">Thailand</option>
<option value="33">Togo</option>
<option value="207">Tokelau</option>
<option value="208">Tonga</option>
<option value="209">Trinidad and Tobago</option>
<option value="210">Tunisia</option>
<option value="211">Turkey</option>
<option value="212">Turkmenistan</option>
<option value="213">Turks and Caicos Islands</option>
<option value="214">Tuvalu</option>
<option value="215">Uganda</option>
<option value="216">Ukraine</option>
<option value="217">United Arab Emirates</option>
<option value="17">United Kingdom</option>
<option value="21">United States</option>
<option value="218">Uruguay</option>
<option value="219">Uzbekistan</option>
<option value="220">Vanuatu</option>
<option value="107">Vatican City State</option>
<option value="221">Venezuela</option>
<option value="222">Vietnam</option>
<option value="223">Virgin Islands (British)</option>
<option value="224">Virgin Islands (U.S.)</option>
<option value="225">Wallis and Futuna</option>
<option value="226">Western Sahara</option>
<option value="227">Yemen</option>
<option value="228">Zambia</option>
<option value="229">Zimbabwe</option>
</select></div>
</div>
<div class="required dni form-group" style="display: block;">
<label for="dni">Número de indentificación fiscal <sup>*</sup></label>
<input type="text" class="text form-control validate uniform-input" name="dni" id="dni" data-validate="isDniLite" value="">
<span class="form_info">DNI/NIF/NIE</span>
</div>
<div class="form-group is_customer_param">
<label for="other">Información adicional</label>
<textarea class="form-control" name="other" id="other" cols="26" rows="7"></textarea>
</div>
<p class="inline-infos required is_customer_param">Debe registrar por lo menos un número telefónico</p>
<div class="form-group is_customer_param">
<label for="phone">Teléfono fijo</label>
<input type="text" class="text form-control validate" name="phone" id="phone" data-validate="isPhoneNumber" value="">
</div>
<div class="required form-group">
<label for="phone_mobile">Teléfono móvil <sup>*</sup></label>
<input type="text" class="text form-control validate" name="phone_mobile" id="phone_mobile" data-validate="isPhoneNumber" value="">
</div>
<input type="hidden" name="alias" id="alias" value="Mi dirección">
<div class="checkbox">
<label for="invoice_address">
<div class="checker" id="uniform-invoice_address"><span><input type="checkbox" name="invoice_address" id="invoice_address" autocomplete="off"></span></div>
Deseo utilizar otra dirección para la facturación
</label>
</div>
<div id="opc_invoice_address" class="is_customer_param" style="display: none;">
<h3 class="page-subheading top-indent">Dirección de facturación</h3>
<div class="required form-group">
<label for="firstname_invoice">Nombre <sup>*</sup></label>
<input type="text" class="form-control validate" id="firstname_invoice" name="firstname_invoice" data-validate="isName" value="">
</div>
<div class="required form-group">
<label for="lastname_invoice">Apellido <sup>*</sup></label>
<input type="text" class="form-control validate" id="lastname_invoice" name="lastname_invoice" data-validate="isName" value="">
</div>
<div class="form-group">
<label for="company_invoice">Empresa</label>
<input type="text" class="text form-control validate" id="company_invoice" name="company_invoice" data-validate="isName" value="">
</div>
<div class="required form-group">
<label for="address1_invoice">Dirección <sup>*</sup></label>
<input type="text" class="form-control validate" name="address1_invoice" id="address1_invoice" data-validate="isAddress" value="">
</div>
<div class="form-group is_customer_param">
<label for="address2_invoice">Dirección (2)</label>
<input type="text" class="form-control address" name="address2_invoice" id="address2_invoice" data-validate="isAddress" value="">
</div>
<div class="required form-group">
<label for="city_invoice">Ciudad <sup>*</sup></label>
<input type="text" class="form-control validate" name="city_invoice" id="city_invoice" data-validate="isCityName" value="">
</div>
<div class="required id_state_invoice form-group" style="display: none;">
<label for="id_state_invoice">Estado <sup>*</sup></label>
<div class="selector" id="uniform-id_state_invoice" style="width: 48px;"><span style="width: 36px; -webkit-user-select: none;">-</span><select name="id_state_invoice" id="id_state_invoice" class="form-control">
<option value="">-</option>
</select></div>
</div>
<div class="required postcode_invoice form-group">
<label for="postcode_invoice">Código postal <sup>*</sup></label>
<input type="text" class="form-control validate" name="postcode_invoice" id="postcode_invoice" data-validate="isPostCode" value="" onkeyup="$('#postcode_invoice').val($('#postcode_invoice').val().toUpperCase());">
</div>
<div class="required form-group">
<label for="id_country_invoice">País <sup>*</sup></label>
<div class="selector" id="uniform-id_country_invoice" style="width: 31px;"><span style="width: 19px; -webkit-user-select: none;">Spain</span><select name="id_country_invoice" id="id_country_invoice" class="form-control">
<option value="">-</option>
<option value="231">Afghanistan</option>
<option value="244">Åland Islands</option>
<option value="230">Albania</option>
<option value="38">Algeria</option>
<option value="39">American Samoa</option>
<option value="40">Andorra</option>
<option value="41">Angola</option>
<option value="42">Anguilla</option>
<option value="232">Antarctica</option>
<option value="43">Antigua and Barbuda</option>
<option value="44">Argentina</option>
<option value="45">Armenia</option>
<option value="46">Aruba</option>
<option value="24">Australia</option>
<option value="2">Austria</option>
<option value="47">Azerbaijan</option>
<option value="48">Bahamas</option>
<option value="49">Bahrain</option>
<option value="50">Bangladesh</option>
<option value="51">Barbados</option>
<option value="52">Belarus</option>
<option value="3">Belgium</option>
<option value="53">Belize</option>
<option value="54">Benin</option>
<option value="55">Bermuda</option>
<option value="56">Bhutan</option>
<option value="34">Bolivia</option>
<option value="233">Bosnia and Herzegovina</option>
<option value="57">Botswana</option>
<option value="234">Bouvet Island</option>
<option value="58">Brazil</option>
<option value="235">British Indian Ocean Territory</option>
<option value="59">Brunei</option>
<option value="236">Bulgaria</option>
<option value="60">Burkina Faso</option>
<option value="61">Burma (Myanmar)</option>
<option value="62">Burundi</option>
<option value="63">Cambodia</option>
<option value="64">Cameroon</option>
<option value="4">Canada</option>
<option value="65">Cape Verde</option>
<option value="237">Cayman Islands</option>
<option value="66">Central African Republic</option>
<option value="67">Chad</option>
<option value="68">Chile</option>
<option value="5">China</option>
<option value="238">Christmas Island</option>
<option value="239">Cocos (Keeling) Islands</option>
<option value="69">Colombia</option>
<option value="70">Comoros</option>
<option value="71">Congo, Dem. Republic</option>
<option value="72">Congo, Republic</option>
<option value="240">Cook Islands</option>
<option value="73">Costa Rica</option>
<option value="74">Croatia</option>
<option value="75">Cuba</option>
<option value="76">Cyprus</option>
<option value="16">Czech Republic</option>
<option value="20">Denmark</option>
<option value="77">Djibouti</option>
<option value="78">Dominica</option>
<option value="79">Dominican Republic</option>
<option value="80">East Timor</option>
<option value="81">Ecuador</option>
<option value="82">Egypt</option>
<option value="83">El Salvador</option>
<option value="84">Equatorial Guinea</option>
<option value="85">Eritrea</option>
<option value="86">Estonia</option>
<option value="87">Ethiopia</option>
<option value="88">Falkland Islands</option>
<option value="89">Faroe Islands</option>
<option value="90">Fiji</option>
<option value="7">Finland</option>
<option value="8">France</option>
<option value="241">French Guiana</option>
<option value="242">French Polynesia</option>
<option value="243">French Southern Territories</option>
<option value="91">Gabon</option>
<option value="92">Gambia</option>
<option value="93">Georgia</option>
<option value="1">Germany</option>
<option value="94">Ghana</option>
<option value="97">Gibraltar</option>
<option value="9">Greece</option>
<option value="96">Greenland</option>
<option value="95">Grenada</option>
<option value="98">Guadeloupe</option>
<option value="99">Guam</option>
<option value="100">Guatemala</option>
<option value="101">Guernsey</option>
<option value="102">Guinea</option>
<option value="103">Guinea-Bissau</option>
<option value="104">Guyana</option>
<option value="105">Haiti</option>
<option value="106">Heard Island and McDonald Islands</option>
<option value="108">Honduras</option>
<option value="22">HongKong</option>
<option value="143">Hungary</option>
<option value="109">Iceland</option>
<option value="110">India</option>
<option value="111">Indonesia</option>
<option value="112">Iran</option>
<option value="113">Iraq</option>
<option value="26">Ireland</option>
<option value="29">Israel</option>
<option value="10">Italy</option>
<option value="32">Ivory Coast</option>
<option value="115">Jamaica</option>
<option value="11">Japan</option>
<option value="116">Jersey</option>
<option value="117">Jordan</option>
<option value="118">Kazakhstan</option>
<option value="119">Kenya</option>
<option value="120">Kiribati</option>
<option value="121">Korea, Dem. Republic of</option>
<option value="122">Kuwait</option>
<option value="123">Kyrgyzstan</option>
<option value="124">Laos</option>
<option value="125">Latvia</option>
<option value="126">Lebanon</option>
<option value="127">Lesotho</option>
<option value="128">Liberia</option>
<option value="129">Libya</option>
<option value="130">Liechtenstein</option>
<option value="131">Lithuania</option>
<option value="12">Luxemburg</option>
<option value="132">Macau</option>
<option value="133">Macedonia</option>
<option value="134">Madagascar</option>
<option value="135">Malawi</option>
<option value="136">Malaysia</option>
<option value="137">Maldives</option>
<option value="138">Mali</option>
<option value="139">Malta</option>
<option value="114">Man Island</option>
<option value="140">Marshall Islands</option>
<option value="141">Martinique</option>
<option value="142">Mauritania</option>
<option value="35">Mauritius</option>
<option value="144">Mayotte</option>
<option value="145">Mexico</option>
<option value="146">Micronesia</option>
<option value="147">Moldova</option>
<option value="148">Monaco</option>
<option value="149">Mongolia</option>
<option value="150">Montenegro</option>
<option value="151">Montserrat</option>
<option value="152">Morocco</option>
<option value="153">Mozambique</option>
<option value="154">Namibia</option>
<option value="155">Nauru</option>
<option value="156">Nepal</option>
<option value="13">Netherlands</option>
<option value="157">Netherlands Antilles</option>
<option value="158">New Caledonia</option>
<option value="27">New Zealand</option>
<option value="159">Nicaragua</option>
<option value="160">Niger</option>
<option value="31">Nigeria</option>
<option value="161">Niue</option>
<option value="162">Norfolk Island</option>
<option value="163">Northern Mariana Islands</option>
<option value="23">Norway</option>
<option value="164">Oman</option>
<option value="165">Pakistan</option>
<option value="166">Palau</option>
<option value="167">Palestinian Territories</option>
<option value="168">Panama</option>
<option value="169">Papua New Guinea</option>
<option value="170">Paraguay</option>
<option value="171">Peru</option>
<option value="172">Philippines</option>
<option value="173">Pitcairn</option>
<option value="14">Poland</option>
<option value="15">Portugal</option>
<option value="174">Puerto Rico</option>
<option value="175">Qatar</option>
<option value="176">Reunion Island</option>
<option value="36">Romania</option>
<option value="177">Russian Federation</option>
<option value="178">Rwanda</option>
<option value="179">Saint Barthelemy</option>
<option value="180">Saint Kitts and Nevis</option>
<option value="181">Saint Lucia</option>
<option value="182">Saint Martin</option>
<option value="183">Saint Pierre and Miquelon</option>
<option value="184">Saint Vincent and the Grenadines</option>
<option value="185">Samoa</option>
<option value="186">San Marino</option>
<option value="187">São Tomé and Príncipe</option>
<option value="188">Saudi Arabia</option>
<option value="189">Senegal</option>
<option value="190">Serbia</option>
<option value="191">Seychelles</option>
<option value="192">Sierra Leone</option>
<option value="25">Singapore</option>
<option value="37">Slovakia</option>
<option value="193">Slovenia</option>
<option value="194">Solomon Islands</option>
<option value="195">Somalia</option>
<option value="30">South Africa</option>
<option value="196">South Georgia and the South Sandwich Islands</option>
<option value="28">South Korea</option>
<option value="6" selected="selected">Spain</option>
<option value="197">Sri Lanka</option>
<option value="198">Sudan</option>
<option value="199">Suriname</option>
<option value="200">Svalbard and Jan Mayen</option>
<option value="201">Swaziland</option>
<option value="18">Sweden</option>
<option value="19">Switzerland</option>
<option value="202">Syria</option>
<option value="203">Taiwan</option>
<option value="204">Tajikistan</option>
<option value="205">Tanzania</option>
<option value="206">Thailand</option>
<option value="33">Togo</option>
<option value="207">Tokelau</option>
<option value="208">Tonga</option>
<option value="209">Trinidad and Tobago</option>
<option value="210">Tunisia</option>
<option value="211">Turkey</option>
<option value="212">Turkmenistan</option>
<option value="213">Turks and Caicos Islands</option>
<option value="214">Tuvalu</option>
<option value="215">Uganda</option>
<option value="216">Ukraine</option>
<option value="217">United Arab Emirates</option>
<option value="17">United Kingdom</option>
<option value="21">United States</option>
<option value="218">Uruguay</option>
<option value="219">Uzbekistan</option>
<option value="220">Vanuatu</option>
<option value="107">Vatican City State</option>
<option value="221">Venezuela</option>
<option value="222">Vietnam</option>
<option value="223">Virgin Islands (British)</option>
<option value="224">Virgin Islands (U.S.)</option>
<option value="225">Wallis and Futuna</option>
<option value="226">Western Sahara</option>
<option value="227">Yemen</option>
<option value="228">Zambia</option>
<option value="229">Zimbabwe</option>
</select></div>
</div>
<div class="required form-group dni_invoice">
<label for="dni">Número de indentificación fiscal <sup>*</sup></label>
<input type="text" class="text form-control validate" name="dni_invoice" id="dni_invoice" data-validate="isDniLite" value="">
<span class="form_info">DNI/NIF/NIE</span>
</div>
<div class="form-group is_customer_param">
<label for="other_invoice">Información adicional</label>
<textarea class="form-control" name="other_invoice" id="other_invoice" cols="26" rows="3"></textarea>
</div>
<p class="inline-infos required is_customer_param">Debe registrar por lo menos un número telefónico</p>
<div class="form-group is_customer_param">
<label for="phone_invoice">Teléfono fijo</label>
<input type="text" class="form-control validate" name="phone_invoice" id="phone_invoice" data-validate="isPhoneNumber" value="">
</div>
<div class="required form-group">
<label for="phone_mobile_invoice">Teléfono móvil <sup>*</sup></label>
<input type="text" class="form-control validate" name="phone_mobile_invoice" id="phone_mobile_invoice" data-validate="isPhoneNumber" value="">
</div>
<input type="hidden" name="alias_invoice" id="alias_invoice" value="Mi dirección de facturación">
</div>
<div class="submit opc-add-save clearfix">
<p class="required opc-required pull-right">
<sup>*</sup>Campo requerido
</p>
<button type="submit" name="submitAccount" id="submitAccount" class="btn btn-default btn-md icon-right"><span>Guardar</span></button>
</div>
<div style="display: none;" id="opc_account_saved" class="alert alert-success">
Información de cuenta guardada exitosamente.
</div>
 
</div>
</div>
</fieldset>
</form>
</div>
 
 
<div id="carrier_area" class="opc-main-block">
<h1 class="page-heading step-num"><span>2</span> Formas de entrega</h1>
<div id="opc_delivery_methods" class="opc-main-block">
<div id="opc_delivery_methods-overlay" class="opc-overlay" style="display: none;"></div>
<div class="order_carrier_content box">
<div id="HOOK_BEFORECARRIER">
</div>
<div class="checkbox">
<label for="recyclable">
<div class="checker" id="uniform-recyclable"><span><input type="checkbox" name="recyclable" id="recyclable" value="1"></span></div>
Acepto recibir mi pedido en embalaje reciclado.
</label>
</div>
<div class="delivery_options_address">
<p class="carrier_title">
Seleccione una opción de envío
</p>
<div class="delivery_options">
<div class="delivery_option item">
<div>
<table class="resume table table-bordered">
<tbody><tr>
<td class="delivery_option_radio">
<div class="radio" id="uniform-delivery_option_0_0"><span class="checked"><input id="delivery_option_0_0" class="delivery_option_radio" type="radio" name="delivery_option[0]" data-key="5," data-id_address="0" value="5," checked="checked"></span></div>
</td>
<td class="delivery_option_logo">
<img src="/prestashop_53429/img/s/5.jpg" alt="new store">
</td>
<td>
<strong>new store</strong>
Pick up in-store
<span class="best_grade best_grade_price best_grade_speed">El mejor precio y tiempo</span>
</td>
<td class="delivery_option_price">
<div class="delivery_option_price">
¡Gratis!
</div>
</td>
</tr>
</tbody></table>
</div></div>   <div class="delivery_option alternate_item">
<div>
<table class="resume table table-bordered">
<tbody><tr>
<td class="delivery_option_radio">
<div class="radio" id="uniform-delivery_option_0_1"><span><input id="delivery_option_0_1" class="delivery_option_radio" type="radio" name="delivery_option[0]" data-key="6," data-id_address="0" value="6,"></span></div>
</td>
<td class="delivery_option_logo">
<img src="/prestashop_53429/img/s/6.jpg" alt="My carrier">
</td>
<td>
<strong>My carrier</strong>
Delivery next day!
</td>
<td class="delivery_option_price">
<div class="delivery_option_price">
$7.00 </div>
</td>
</tr>
</tbody></table>
</div></div>  </div>  <div class="hook_extracarrier" id="HOOK_EXTRACARRIER_0"></div></div>  <p class="carrier_title">Dejar un mensaje</p><div><p>Si desea dejarnos un comentario acerca de su pedido, por favor escríbalo a continuación.</p><textarea class="form-control" cols="120" rows="2" name="message" id="message"></textarea>
</div>
<hr style="">
<div id="extra_carrier" style="display: none;"></div>
<p class="carrier_title">Regalo</p>
<p class="checkbox gift">
<div class="checker" id="uniform-gift"><span><input type="checkbox" name="gift" id="gift" value="1" eventcheckboxchange="true"></span></div>
<label for="gift">
Deseo que mi pedido se envuelva en un papel de regalo.
&nbsp;<i>(Coste adicional
<span class="price" id="gift-price">
$2.00
</span>
)
</i>
</label>
</p>
<p id="gift_div" style="display: none;">
<label for="gift_message">Si lo desea puede añadir una nota al regalo:</label>
<textarea rows="2" cols="120" id="gift_message" class="form-control" name="gift_message"></textarea>
</p>
<hr style="">
<p class="carrier_title">Condiciones de servicio</p>
<p class="checkbox">
<div class="checker" id="uniform-cgv"><span><input type="checkbox" name="cgv" id="cgv" value="1"></span></div>
<label for="cgv">He leído y acepto las condiciones generales de venta.</label>
<a href="http://livedemo00.template-help.com/prestashop_53429/index.php?id_cms=3&amp;controller=cms&amp;id_lang=4&amp;content_only=1&amp;live_configurator&amp;theme=theme4&amp;theme_font=" class="iframe" rel="nofollow">Condiciones generales de venta</a>
</p>
</div>  
</div>  
</div>  
 
 
<h1 class="page-heading step-num"><span>3</span> Elija su modo de pago</h1>
<div id="opc_payment_methods" class="opc-main-block">
<div id="opc_payment_methods-overlay" class="opc-overlay" style="display: none;"></div>
<div class="paiement_block">
<div id="HOOK_TOP_PAYMENT"></div>
<div id="opc_payment_methods-content"> <div id="HOOK_PAYMENT">
<p class="warning">Please sign in to see payment methods.</p>
</div>
</div>   </div>  
</div>  
 
</div> 
</div> 
</div> 
</div>